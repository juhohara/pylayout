from itemgroup import ItemGroup
from mapitem import MapItem

class MapCompositionItem(ItemGroup):

    def __init__(self, parent = None):
        super(MapCompositionItem, self).__init__(parent)

    def adjustPos(self):

        childrenBoundRect = self.childrenBoundingRect()

        cot = len(self.childItems())

        if(cot > 1):
            for x in range(cot):
                child = self.childItems()[x]
                child.setPos(0,0)

        for i in range(cot):

            child = self.childItems()[i]

            x = childrenBoundRect.width() - child.boundingRect().width()
            y = childrenBoundRect.height() - child.boundingRect().height()

            """Centralizar o item"""
            if(x > 0):
                x = x / 2
            if(y > 0):
                y = y / 2

            child.setPos(x, y)

    def updateChildSize(self, item):

        #only map item could be resizable inside a item group with a grid
        if(isinstance(item, MapItem) == False):
            return

        if not(item in self._m_spaceBetweenParentChild):
            return

        childSpace = self._m_spaceBetweenParentChild[item]

        currentWidth = self._m_width
        currentHeight = self._m_height

        width = currentWidth - childSpace.width()
        height = currentHeight - childSpace.height()

        if (width < self._m_marginResizePrecision
            or height < self._m_marginResizePrecision):
            return

        #update properties
        item.set_width(width)
        item.set_height(height)
        item.prepareGeometryChange() #update childrenBoundingRect
        item.updateObservers() #resize grid

        self.scene().update()
