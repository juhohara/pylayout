from PyQt4 import QtGui
from PyQt4 import QtCore
from abstractitem import AbstractItem
from mapcompositionitem import MapCompositionItem
from core.utils import Utils
from core.property.property import Property
from core.enumdatatype import EnumDataType

class GridItem(AbstractItem):

    def __init__(self, parent = None):
        super(GridItem, self).__init__(parent)

        self._initProperties()

    def _initProperties(self):

        backgroundColor = QtGui.QColor(255,0,0,100) #red
        subject = 0
        fontName = QtCore.QString("Arial")
        fontSize = 12
        verticalGap = 20
        horizontalGap = 20
        unit = 1. #(meters)

        prop = Property("backgroundColor", backgroundColor, EnumDataType.Color, self)
        self._m_properties.addProperty(prop)
        prop = Property("subject", subject, EnumDataType.StringList, self)
        self._m_properties.addProperty(prop)
        prop = Property("fontName", fontName, EnumDataType.String, self)
        self._m_properties.addProperty(prop)
        prop = Property("fontSize", fontSize, EnumDataType.Integer, self)
        self._m_properties.addProperty(prop)
        prop = Property("verticalGap", verticalGap, EnumDataType.Double, self)
        self._m_properties.addProperty(prop)
        prop = Property("horizontalGap", horizontalGap, EnumDataType.Double, self)
        self._m_properties.addProperty(prop)
        prop = Property("unit", unit, EnumDataType.String, self)
        self._m_properties.addProperty(prop)

    """Implemented from AbstractItem"""
    def drawItem(self, painter, option, widget):
        painter.save()

        prop_backgroundColor = self._m_properties.getProperty("backgroundColor")
        backgroundColor = prop_backgroundColor.get_value()

        if(self._m_modeEdition == True):
            backColor = QtGui.QColor(255,255,0) #yellow
            br = QtGui.QBrush(backColor)
            painter.setBrush(br)
        else:
            br = QtGui.QBrush(backgroundColor)
            painter.setBrush(br)

        boundRect = self.boundingRect()
        painter.drawRect(boundRect)

        #self.drawVertical(painter)

        painter.restore()

    def updateObserver(self, subject):

        boundRect = subject.boundingRect()

        self.prepareGeometryChange()

        rectVertical = self.getVerticalMaxTextBoundingRect(boundRect)
        rectHorizontal = self.getHorizontalMaxTextBoundingRect(boundRect)

        dx = rectVertical.width()
        dy = rectHorizontal.height()

        self._m_width = boundRect.width() + (dx*2)
        self._m_height = boundRect.height() + (dy*2)

        if(self.parentItem()):

            if(isinstance(self.parentItem(), MapCompositionItem) == True):
                self.parentItem().adjustPos()

    def drawVertical(self, painter):

        y1 = 0

        boundRect = self.boundingRect()

        x1 = boundRect.bottomLeft().x()
        x2 = boundRect.bottomRight().x()

        view = self.scene().views()[0]

        scale = view.transform().m11()

        zoomFactor = view.getCurrentZoom() / 100

        qFont = QtGui.QFont(self._m_fontName)
        qFont.setPointSizeF((self._m_fontSize) * zoomFactor)

        painter.save()

        painter.setFont(qFont)

        while(y1 <= boundRect.bottomLeft().y()):

            if(y1 < boundRect.topRight().y()):
                y1 += self._m_verticalGap
                continue

            number = y1 / self._m_unit
            convert = QtCore.QString(str(number))

            boundRectText = self.getMinimumTextBoundary(self._m_fontName, self._m_fontSize, convert)

            newX2 = x2 - boundRectText.width()

            point1 = QtCore.QPointF(x1, y1)
            point2 = QtCore.QPointF(newX2, y1)

            point1 = self.mapToScene(point1)
            point2 = self.mapToScene(point2)

            point1 = view.mapFromScene(point1)
            point2 = view.mapFromScene(point2)

            painter.setMatrixEnabled(False)
            painter.drawText(point1, convert) #left text
            painter.drawText(point2, convert) #right text
            painter.setMatrixEnabled(True)

            y1 += self._m_verticalGap

        painter.restore()


    def getVerticalMaxTextBoundingRect(self, box):

        y1 = 0

        maxBoundingRect = QtCore.QRectF(0,0,0,0)

        prop_unit = self._m_properties.getProperty("unit")
        unit = prop_unit.get_value()

        prop_fontName = self._m_properties.getProperty("fontName")
        fontName = prop_fontName.get_value()

        prop_fontSize = self._m_properties.getProperty("fontSize")
        fontSize = prop_fontSize.get_value()

        prop_verticalGap = self._m_properties.getProperty("verticalGap")
        verticalGap = prop_verticalGap.get_value()

        while(y1 <= box.bottomLeft().y()):

            if(y1 < box.topRight().y()):
                y1 += verticalGap
                continue

            number = y1 / unit
            convert = QtCore.QString(str(number))

            boundRect = self.getMinimumTextBoundary(fontName, fontSize, convert)

            if(boundRect.width() > maxBoundingRect.width()):
                maxBoundingRect.setWidth(boundRect.width())

            if(boundRect.height() > maxBoundingRect.height()):
                maxBoundingRect.setHeight(boundRect.height())

            y1 += verticalGap

        return maxBoundingRect

    def getHorizontalMaxTextBoundingRect(self, box):

        x1 = 0

        maxBoundingRect = QtCore.QRectF(0,0,0,0)

        prop_unit = self._m_properties.getProperty("unit")
        unit = prop_unit.get_value()

        prop_fontName = self._m_properties.getProperty("fontName")
        fontName = prop_fontName.get_value()

        prop_fontSize = self._m_properties.getProperty("fontSize")
        fontSize = prop_fontSize.get_value()

        prop_horizontalGap = self._m_properties.getProperty("horizontalGap")
        horizontalGap = prop_horizontalGap.get_value()

        while(x1 <= box.bottomRight().x()):

            if(x1 < box.topLeft().x()):
                x1 += horizontalGap
                continue

            number = x1 / unit
            convert = QtCore.QString(str(number))

            boundRect = self.getMinimumTextBoundary(fontName, fontSize, convert)

            if(boundRect.width() > maxBoundingRect.width()):
                maxBoundingRect.setWidth(boundRect.width())

            if(boundRect.height() > maxBoundingRect.height()):
                maxBoundingRect.setHeight(boundRect.height())

            x1 += horizontalGap

        return maxBoundingRect

    def getMinimumTextBoundary(self, fontName, fontSize, text):

        correctionFactorY = float(96) / float(72.) #keep aspect

        font = QtGui.QFont(fontName)
        font.setPixelSize(fontSize * correctionFactorY)

        fontMetrics = QtGui.QFontMetrics(font)
        rect = fontMetrics.tightBoundingRect(text)

        width = rect.width()
        height = rect.height()
        descend = fontMetrics.descent()

        widthMM = Utils.pixel2mm(width)
        heightMM = Utils.pixel2mm(height)
        descendMM = Utils.pixel2mm(descend)

        textBoundingRect = QtCore.QRectF(0, -descendMM, widthMM, heightMM)

        return textBoundingRect

    def setSubject(self, subject):

        self._m_subject = subject

    def getSubject(self):

        return self._m_subject

    def set_fontName(self, name):

        self._m_fontName = name

    def get_fontName(self):

        return self._m_fontName

