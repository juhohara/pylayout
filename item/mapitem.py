from PyQt4 import QtGui
from PyQt4 import QtCore
from abstractitem import AbstractItem
from core.property.property import Property
from core.enumdatatype import EnumDataType

class MapItem(AbstractItem):

    def __init__(self, parent = None):
        super(MapItem, self).__init__(parent)

        self._initProperties()

    def _initProperties(self):

        width = 100
        height = 100

        worldBox = QtCore.QRectF(0,0,10,10)
        observer = []
        backgroundColor = QtGui.QColor(0,255,0,100) #green

        prop_width = self._m_properties.getProperty("width")
        prop_width.set_value(width)
        self._m_properties.updateProperty(prop_width)

        prop_height = self._m_properties.getProperty("height")
        prop_width.set_value(height)
        self._m_properties.updateProperty(prop_height)

        prop = Property("backgroundColor", backgroundColor, EnumDataType.Color, self, False)
        self._m_properties.addProperty(prop)

        prop = Property("observers", observer, EnumDataType.StringList, self, False)
        self._m_properties.addProperty(prop)
        prop = Property("world_box", worldBox, EnumDataType.Envelope, self)
        self._m_properties.addProperty(prop)

    """Implemented from AbstractItem"""
    def drawItem(self, painter, option, widget):
        painter.save()

        prop_backgroundColor = self._m_properties.getProperty("backgroundColor")
        backgroundColor = prop_backgroundColor.get_value()

        if(self._m_modeEdition == True):
            backColor = QtGui.QColor(255,255,0) #yellow
            br = QtGui.QBrush(backColor)
            painter.setBrush(br)
        else:
            br = QtGui.QBrush(backgroundColor)
            painter.setBrush(br)

        boundRect = self.boundingRect()
        painter.drawRect(boundRect)

        painter.restore()


    def attach(self, observer):

        exist = False

        prop_observers = self._m_properties.getProperty("observers")
        observers = prop_observers.get_value()

        cot = len(observers)
        for i in range(cot):

            if(observers[i] == observer):
                exist = True

        if(exist):
            return

        observers.append(observer)
        observer.setSubject(self)
        observer.updateObserver(self)

    def updateObservers(self):

        prop_observers = self._m_properties.getProperty("observers")
        observers = prop_observers.get_value()

        cot = len(self._m_observer)
        if(cot > 0):

            for i in range(cot):

                observers[i].updateObserver(self)

    def getObservers(self):

        prop_observers = self._m_properties.getProperty("observers")
        observers = prop_observers.get_value()

        return observers

