import abc
from PyQt4 import QtGui
from PyQt4 import QtCore
from PyQt4.QtCore import Qt
from core.utils import EnumModes
from core.property.properties import Properties
from core.property.property import Property
from core.enumdatatype import EnumDataType

"""
    TPTopLeft, TPTopRight, TPLowerLeft, TPLowerRight, TPNoneSide
"""


class AbstractItem(QtGui.QGraphicsItem):

    def __init__(self, parent = None):
        super(AbstractItem, self).__init__(parent)
        self._m_width = 50
        self._m_height = 30
        self._m_backgroundColor = QtGui.QColor(0,0,255)
        self._m_penColor = QtGui.QColor(0,0,0)
        self._m_inverted = False
        self._m_modeEdition = False
        self._m_enumSides = EnumSides.TPNoneSide
        self._m_marginResizePrecision = 2.
        self._m_keepAspect = False
        self._m_spaceBetweenParentChild = {} #dictionary

        self.setFlag(QtGui.QGraphicsItem.ItemIsMovable, True)
        self.setFlag(QtGui.QGraphicsItem.ItemIsSelectable, True)
        self.setFlag(QtGui.QGraphicsItem.ItemSendsGeometryChanges, True)
        self.setFlag(QtGui.QGraphicsItem.ItemIsFocusable, True)

        #If enabled is true, this item will accept hover events
        self.setAcceptHoverEvents(True)

        self._m_currentAction = EnumModes.NO_ACTION
        self._m_rect = self.boundingRect()
        self._m_initialCoord = self.pos()
        self._m_finalCoord = self.pos()

        self._m_properties = Properties(self)
        AbstractItem._initProperties(self) # init properties

    def _initProperties(self):

        prop = Property("width", self._m_width, EnumDataType.Double, self)
        self._m_properties.addProperty(prop)
        prop = Property("height", self._m_height, EnumDataType.Double, self)
        self._m_properties.addProperty(prop)

        print "AbstractItem::_initProperties..." + str(self._m_width) + " " + str(self._m_height)

    def paint (self, painter, option, widget = None):

        painter.save()

        br = QtGui.QBrush(self._m_backgroundColor)
        painter.setBrush(br)

        pen = QtGui.QPen(self._m_penColor)
        painter.setPen(pen)

        self.drawItem(painter, option, widget) #abstract method

        if (option.state & QtGui.QStyle.State_Selected):
            self.drawSelection(painter)

        painter.restore()

    def itemChange(self, change, variant):

        if change == QtGui.QGraphicsItem.ItemSceneHasChanged:
            #check and inverted transform
            if(self._m_inverted):
                transfView = self.scene().sceneTransform()
                transfItem = self.transform()
                if transfItem != transfView.inverted():
                    transf = transfView.inverted()
                    self.setTransform(transf)

        return super(AbstractItem, self).itemChange(change, variant)

    """Reimplemented from QGraphicsItem"""
    def keyPressEvent(self, event):
        QtGui.QGraphicsItem.keyPressEvent(self, event)

    """Reimplemented from QGraphicsItem"""
    def mouseDoubleClickEvent(self, event):
        QtGui.QGraphicsItem.mouseDoubleClickEvent(self,event)

    """Reimplemented from QGraphicsItem"""
    def mouseMoveEvent(self, event):

        if (self._m_modeEdition == True):
            return

        if (self._m_currentAction == EnumModes.RESIZE_ACTION):

            self.setOpacity(0.5)
            self._m_finalCoord = event.pos()
            self.prepareGeometryChange()
            self.calculateResize()

        else:

            if (event.buttons() == Qt.LeftButton):

                self._m_currentAction = EnumModes.MOVE_ACTION

            QtGui.QGraphicsItem.mouseMoveEvent(self,event)

    """Reimplemented from QGraphicsItem"""
    def mousePressEvent(self, event):

        """If so, checks if the resize operation must be started"""
        startResizing = self.checkTouchesCorner(event.pos().x(), event.pos().y())
        if (startResizing == True):
            self._m_currentAction = EnumModes.RESIZE_ACTION
            self._m_rect = self.boundingRect()
            self._m_initialCoord = event.pos()
            self.beginResize()

        QtGui.QGraphicsItem.mousePressEvent(self,event)

    """Reimplemented from QGraphicsItem"""
    def mouseReleaseEvent(self, event):

        if (self._m_currentAction == EnumModes.RESIZE_ACTION):

            self._m_finalCoord = event.pos()
            self.calculateResize()
            newPos = QtCore.QPointF(self._m_rect.x(), self._m_rect.y())
            newPos = self.mapToParent(newPos)

            self.setPos(newPos)
            self._m_rect.moveTo(0, 0)
            self.setOpacity(1.)

            self._m_width = self._m_rect.width()
            self._m_height = self._m_rect.height()

            self.endResize()

        self._m_currentAction = EnumModes.NO_ACTION
        QtGui.QGraphicsItem.mouseReleaseEvent(self,event)

    """Reimplemented from QGraphicsItem"""
    def hoverEnterEvent(self, event):
        QtGui.QGraphicsItem.hoverEnterEvent(self,event)

    """Reimplemented from QGraphicsItem"""
    def hoverLeaveEvent(self, event):
        QtGui.QGraphicsItem.hoverLeaveEvent(self,event)

    """Reimplemented from QGraphicsItem"""
    def hoverMoveEvent(self, event):

        if(self._m_modeEdition == False):
            self.checkTouchesCorner(event.pos().x(), event.pos().y())

        QtGui.QGraphicsItem.hoverMoveEvent(self,event)

    """Implemented from QGraphicsItem"""
    def boundingRect(self):

        if (self._m_currentAction == EnumModes.RESIZE_ACTION):
            return self._m_rect

        return QtCore.QRectF(0, 0, self._m_width, self._m_height)

    def checkTouchesCorner(self, x, y):

        result = True
        bRect = self.boundingRect()

        ll = bRect.bottomLeft()
        lr = bRect.bottomRight()
        tl = bRect.topLeft()
        tr = bRect.topRight()

        if ((x >= (ll.x() - self._m_marginResizePrecision) and x <= (ll.x() + self._m_marginResizePrecision))
        and (y >= (ll.y() - self._m_marginResizePrecision) and y <= (ll.y() + self._m_marginResizePrecision))):

            self.setCursor(Qt.SizeFDiagCursor)
            self._m_enumSides = EnumSides.TPTopLeft

        elif ((x >= (lr.x() - self._m_marginResizePrecision) and x <= (lr.x() + self._m_marginResizePrecision))
        and (y >= (lr.y() - self._m_marginResizePrecision) and y <= (lr.y() + self._m_marginResizePrecision))):

            self.setCursor(Qt.SizeBDiagCursor)
            self._m_enumSides = EnumSides.TPTopRight

        elif ((x >= (tl.x() - self._m_marginResizePrecision) and x <= (tl.x() + self._m_marginResizePrecision))
        and (y >= (tl.y() - self._m_marginResizePrecision) and y <= (tl.y() + self._m_marginResizePrecision))):

            self.setCursor(Qt.SizeBDiagCursor)
            self._m_enumSides = EnumSides.TPLowerLeft

        elif ((x >= (tr.x() - self._m_marginResizePrecision) and x <= (tr.x() + self._m_marginResizePrecision))
        and (y >= (tr.y() - self._m_marginResizePrecision) and y <= (tr.y() + self._m_marginResizePrecision))):

            self.setCursor(Qt.SizeFDiagCursor)
            self._m_enumSides = EnumSides.TPLowerRight

        else:

            self.setCursor(Qt.ArrowCursor)
            self._m_enumSides = EnumSides.TPNoneSide
            result = False

        return result

    """Abstract Method"""
    @abc.abstractmethod
    def drawItem(self, painter, option, widget):
        """Method documentation"""
        pass

    def getProperties(self):

        return self._m_properties

    def updateProperty(self, prop):

        return self._m_properties.updateProperty(prop)

    def drawSelection(self, painter):

        painter.save()

        penWidth = painter.pen().widthF()

        adj = penWidth / 2
        fgcolor = QtGui.QColor(0,255,0)
        backgroundColor = QtGui.QColor(0,0,0)

        rtAdjusted = self.boundingRect().adjusted(adj, adj, -adj, -adj)

        penBackground = QtGui.QPen(backgroundColor, 0, Qt.SolidLine)
        painter.setPen(penBackground)
        painter.setBrush(Qt.NoBrush)
        painter.drawRect(rtAdjusted)

        penForeground = QtGui.QPen(fgcolor, 0, Qt.DashLine)
        painter.setPen(penForeground)
        painter.setBrush(Qt.NoBrush)
        painter.drawRect(rtAdjusted)

        painter.restore()

    def calculateResize(self):

        if (self._m_currentAction != EnumModes.RESIZE_ACTION):
            return

        resizeRect = self._m_rect

        width = self._m_width
        height = self._m_height
        keepAspect = self._m_keepAspect
        factor = width / height

        finalCoord = self._m_finalCoord

        dx = self._m_finalCoord.x() - self._m_initialCoord.x()
        dy = self._m_finalCoord.y() - self._m_initialCoord.y()

        correctionX = dx

        if (keepAspect == True):

            correctionX = (dy * factor)

        if (self._m_enumSides == EnumSides.TPTopRight):

            finalCoord.setX(self._m_initialCoord.x() + correctionX)
            resizeRect.setBottomRight(finalCoord)

        elif (self._m_enumSides == EnumSides.TPTopLeft):

            if (keepAspect == True):
                finalCoord.setX(self._m_initialCoord.x() - correctionX)
            else:
                finalCoord.setX(self._m_initialCoord.x() + correctionX)
            resizeRect.setBottomLeft(finalCoord);

        elif (self._m_enumSides == EnumSides.TPLowerRight):

            if (keepAspect == True):
                finalCoord.setX(self._m_initialCoord.x() - correctionX)
            else:
                finalCoord.setX(self._m_initialCoord.x() + correctionX)

            resizeRect.setTopRight(finalCoord)

        elif (self._m_enumSides == EnumSides.TPLowerLeft):

            finalCoord.setX(self._m_initialCoord.x() + correctionX)
            resizeRect.setTopLeft(finalCoord)


        if (self.isLimitExceeded(resizeRect) == False):
            self._m_rect = resizeRect


    def isLimitExceeded(self, resizeRect):

        result = False

        x = resizeRect.topLeft().x()
        y = resizeRect.topLeft().y()

        if ((resizeRect.width() - self._m_marginResizePrecision) <= 0
        or (resizeRect.height() - self._m_marginResizePrecision) <= 0):
            result = True

        return result

    def beginResize(self):

        self._m_spaceBetweenParentChild.clear() # clear dictionary
        cot = len(self.childItems())
        for i in range(cot):

            child = self.childItems().pop(i)

            if(isinstance(child, AbstractItem) == True):

                boundRect = child.boundingRect()
                width = self.childrenBoundingRect().width() - boundRect.width()
                height = self.childrenBoundingRect().height() - boundRect.height()
                self._m_spaceBetweenParentChild[child] = QtCore.QSize(width, height)

    def endResize(self):

        childrenBoundRect = self.childrenBoundingRect()

        if (self._m_rect != childrenBoundRect):
            self.updateChildren()

        cot = len(self._m_spaceBetweenParentChild)
        if(cot > 0):
            self.update()

        self._m_spaceBetweenParentChild.clear()

    def updateChildren(self):

        cot = len(self.childItems())
        for i in range(cot):

          child = self.childItems()[i]

          if(isinstance(child, AbstractItem) == True):

            self.updateChildSize(child)

    def updateChildSize(self, item):

        if not(item in self._m_spaceBetweenParentChild):
            return

        childSpace = self._m_spaceBetweenParentChild[item]

        currentWidth = self._m_width
        currentHeight = self._m_height

        width = currentWidth - childSpace.width()
        height = currentHeight - childSpace.height()

        if (width < self._m_marginResizePrecision
            or height < self._m_marginResizePrecision):
            return

        #update properties
        item.set_width(width)
        item.set_height(height)
        item.prepareGeometryChange() #update childrenBoundingRect

    def get_width(self):
        return self._m_width

    def set_width(self, v):
        self._m_width = v

    def get_height(self):
        return self._m_height

    def set_height(self, v):
        self._m_height = v

    def get_backgroundColor(self):
        return self._m_backgroundColor

    def set_backgroundColor(self, v):
        self._m_backgroundColor = v

    def get_penColor(self):
        return self._m_penColor

    def set_penColor(self, v):
        self._m_penColor = v

    def is_modeEdition(self):
        return self._m_modeEdition

    def set_modeEdition(self, v):
        self._m_modeEdition = v

    def hello(self):
        print "Hello, I'm: " + self.__class__.__name__
        print "Am I in edition mode?: " + str(self._m_modeEdition)

class EnumSides:
    TPTopLeft, TPTopRight, TPLowerLeft, TPLowerRight, TPNoneSide = range(5)