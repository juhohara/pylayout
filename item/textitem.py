from PyQt4 import QtGui
from PyQt4 import QtCore
from abstractitem import AbstractItem
from core.property.properties import Properties
from core.property.property import Property
from core.enumdatatype import EnumDataType

class TextItem(QtGui.QGraphicsTextItem, AbstractItem):

    def __init__(self, parent=None):
        super(TextItem, self).__init__(parent)
        self.__m_isInEdition = False
        self.setAcceptHoverEvents(False)
        self._m_inverted = True

        self.setPlainText("Teste_01")

        # Connect the contentsChange signal to a slot.
        self.document().contentsChange.connect(self.updateGeometry)

        self._initProperties()

    def _initProperties(self):

        prop = Property("text", self.toPlainText(), EnumDataType.String, self)
        font = QtGui.QFont("Verdana", 10) # font settings
        prop_child = Property("font", font, EnumDataType.FontSettings, self)
        prop.set_child(prop_child)
        self._m_properties.addProperty(prop)

    """Implemented from AbstractItem"""
    def drawItem(self, painter, option, widget):

        if(self._m_modeEdition == True):
            backColor = QtGui.QColor(255,255,0) #yellow
            br = QtGui.QBrush(backColor)
            painter.setBrush(br)
            painter.drawRect(self.boundingRect())

        QtGui.QGraphicsTextItem.paint(self, painter, option, widget)

    def itemChange(self, change, variant):
        if change == QtGui.QGraphicsItem.ItemSelectedHasChanged:
            if self.__m_isInEdition and self.isSelected() == False:
                self.leaveEditionMode()

        return super(TextItem, self).itemChange(change, variant)

    """
        Reimplemented from QGraphicsItem.
        Necessario para o modo de edicao da cena funcionar
        para a edicao do texto (captura das teclas).
    """
    def keyPressEvent(self, event):
        super(TextItem, self).keyPressEvent(event)

    """Reimplemented from AbstractItem"""
    def mouseDoubleClickEvent(self, event):
        if (event.button() == QtCore.Qt.LeftButton):
            if (self.__m_isInEdition == False):
                self.enterEditionMode()
            else:
                self.setCursor(QtCore.Qt.ArrowCursor)

        super(TextItem, self).mouseDoubleClickEvent(event)

    """Reimplemented from AbstractItem"""
    def boundingRect(self):
        """ when we are editing the item, we let the item handle the changes in the bounding box """
        return QtGui.QGraphicsTextItem.boundingRect(self)

    def enterEditionMode(self):

        if (self.__m_isInEdition == True):
            return

        # If enabled is true, this item will accept hover events
        self.setTextInteractionFlags(QtCore.Qt.TextEditorInteraction)
        self.setCursor(QtCore.Qt.IBeamCursor)

        cursor = QtGui.QTextCursor(self.textCursor())
        cursor.clearSelection()

        self.setTextCursor(cursor)
        self.setFocus()

        self.__m_isInEdition = True

    def leaveEditionMode(self):

        if (self.__m_isInEdition == False):
            return

        """Necessary clear the selection and focus of the edit
           after being completely closed and like this not cause bad behavior."""
        cursor = QtGui.QTextCursor(self.textCursor())
        cursor.clearSelection()
        self.setTextCursor(cursor)
        self.setTextInteractionFlags(QtCore.Qt.NoTextInteraction)
        self.unsetCursor()
        self.clearFocus()

        self.__m_isInEdition = False

    def updateGeometry(self, position, charsRemoved, charsAdded):

        self.setTextWidth(-1)
        self.setTextWidth(self.boundingRect().width())




