from PyQt4 import QtGui
from abstractitem import AbstractItem
from core.property.property import Property
from core.enumdatatype import EnumDataType

class RectangleItem(AbstractItem):

    def __init__(self, parent = None):
        super(RectangleItem, self).__init__(parent)

        self._initProperties()

    def _initProperties(self):

        width = 30
        height = 30

        prop_width = self._m_properties.getProperty("width")
        prop_width.set_value(width)
        self._m_properties.updateProperty(prop_width)

        prop_height = self._m_properties.getProperty("height")
        prop_width.set_value(height)
        self._m_properties.updateProperty(prop_height)

        text = "rectangle_item"
        prop = Property("text", text, EnumDataType.String, self)
        text = "child"
        prop_child = Property("text_child", text, EnumDataType.String, self)
        prop.set_child(prop_child)
        self._m_properties.addProperty(prop)

        return self._m_properties

    """Implemented from AbstractItem"""
    def drawItem(self, painter, option, widget):
        painter.save()

        if(self._m_modeEdition == True):
            backColor = QtGui.QColor(255,255,0) #yellow
            br = QtGui.QBrush(backColor)
            painter.setBrush(br)

        boundRect = self.boundingRect()
        painter.drawRect(boundRect)

        painter.restore()





