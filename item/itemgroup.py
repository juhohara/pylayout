from PyQt4 import QtGui
from abstractitem import AbstractItem
from core.utils import EnumModes

class ItemGroup(QtGui.QGraphicsItemGroup, AbstractItem):

    def __init__(self, parent = None):
        super(ItemGroup, self).__init__(parent)

        self.setHandlesChildEvents(True)

    def itemChange(self, change, variant):

        if change == QtGui.QGraphicsItem.ItemChildAddedChange:

            #QVariant cast
            child = variant.toPyObject()
            child.setFlag(QtGui.QGraphicsItem.ItemStacksBehindParent, True)
            self._m_rect = self.boundingRect()

        elif change == QtGui.QGraphicsItem.ItemChildRemovedChange:

            child = variant.toPyObject()
            child.setFlag(QtGui.QGraphicsItem.ItemStacksBehindParent, False)
            self._m_rect = self.boundingRect()

        return super(ItemGroup, self).itemChange(change, variant)

    """Implemented from QGraphicsItem"""
    def boundingRect(self):

        if(self._m_currentAction == EnumModes.RESIZE_ACTION):
            return AbstractItem.boundingRect(self)

        cot = len(self.childItems())
        if(cot > 0):
            return self.childrenBoundingRect()
        else:
            return QtGui.QGraphicsItemGroup.boundingRect(self)

