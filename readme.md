# About

**This source code is the result of a prototype for developing and testing solutions for the layout module (C++) of TerraLib 5**

Description: Instructions for setting up the environment for the PyLayout (Prototype in Python/PyQt4 for new features and functions to be added in the Layout module of the TerraLib 5.).

GitLab [TerraPrint (Layout Module)](https://gitlab.dpi.inpe.br/terralib/terraprint)

To run this project you must have installed **Python 2.7** and **PyQt 4**

###### Python 2.7

###### PyQt4

- [Python Package Index](https://pypi.python.org/pypi/PyQt4)
- [PyQt4 Binary Packages (Windows)](https://riverbankcomputing.com/software/pyqt/download)


