from PyQt4 import QtGui
from PyQt4 import QtCore
from core.enumdatatype import EnumDataType

"""
    QTreeView no has a predefined model
    Fonte: http://stackoverflow.com/questions/27898718/multi-level-qtreeview
"""

class DataTreeViewModel(QtGui.QStandardItemModel):

    def __init__(self, view, parent = None):

        super(DataTreeViewModel, self).__init__(parent)


    def data(self, index, role):

        if not(index.isValid()):
            return QtCore.QVariant()

        if(role <= QtCore.Qt.UserRole):
            return QtGui.QStandardItemModel.data(self, index, role)

        if(index.column() == 1):

            variant = index.data(EnumDataType.Envelope)
            if(variant.isValid()):
                return self.envelopeData(self, index, role)

            return QtCore.QVariant()

    def envelopeData(self, index, role):

        value = index.data(role)
        rect = value.toRectF()

        return QtCore.QString("[(%1, %2), %3 x %4]").arg(QtCore.QString.number(rect.x()))\
                                                    .arg(QtCore.QString.number(rect.y()))\
                                                    .arg(QtCore.QString.number(rect.width()))\
                                                    .arg(QtCore.QString.number(rect.height()))
