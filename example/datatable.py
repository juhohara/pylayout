from PyQt4 import QtGui
from core.enumdatatype import EnumDataType

"""
    Example with DataTable
"""

class DataTable(QtGui.QTableWidget):

    def __init__(self, view, parent = None):

        super(DataTable, self).__init__(parent)

        self.verticalHeader().setVisible(False)

    def load(self, properties):

        props = properties.getProperties()
        rows = properties.count()

        columns = 3

        self.configTable(rows, columns)

        for i, prop in enumerate(props):

            propertyName = prop.name
            newItemName = QtGui.QTableWidgetItem(propertyName)
            propertyValue = prop.value
            newItemValue = QtGui.QTableWidgetItem(propertyValue)

            # First Column
            self.setItem(i, 0, newItemName) # Property Name

            if(prop.get_type() == EnumDataType.Integer):

                # Second Column
                cellItem = QtGui.QSpinBox(self)
                cellItem.setValue(propertyValue)
                self.setCellWidget(i, 1, cellItem) # Property Value

            elif (prop.get_type() == EnumDataType.Double):

                # Second Column
                cellItem = QtGui.QDoubleSpinBox(self)
                cellItem.setValue(propertyValue)
                self.setCellWidget(i, 1, cellItem) # Property Value

            elif (prop.get_type() == EnumDataType.String):

                # Second Column
                cellItem = QtGui.QLineEdit(self)
                cellItem.setText(propertyValue)
                self.setCellWidget(i, 1, cellItem) # Property Value

            # Third Column
            cellItem = QtGui.QComboBox(self)
            comboList = ['item1', 'item2', 'item3']
            cellItem.addItems(comboList)
            self.setCellWidget(i, 2, cellItem) # Property Value

        self.resizeColumnsToContents()
        self.resizeRowsToContents()

    #def reload(self, properties):

    def configTable(self, row, col):

        self.setRowCount(row)
        self.setColumnCount(col)

        horHeaders = []
        horHeaders.append("Property")
        horHeaders.append("Value")
        horHeaders.append("StringList")
        self.setHorizontalHeaderLabels(horHeaders)


    def clearAll(self):

        while (self.rowCount() > 0):

            self.removeRow(0)