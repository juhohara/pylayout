import types
from PyQt4 import QtGui
from PyQt4 import QtCore
from core.enumdatatype import EnumDataType

"""
    QTreeWidget has a predefined model
    Fonte: http://stackoverflow.com/questions/27898718/multi-level-qtreeview
    http://blogs.candoerz.com/question/151805/customizing-qt-qtreeview-with-custom-widgets.aspx
"""

"""
    Bom: http://stackoverflow.com/questions/34769640/qt-how-to-show-combo-boxes-as-cells-in-a-tree-view
    QTreeView and QCombobox: http://www.qtcentre.org/threads/43148-QComboBox-in-QTreeView
    The Boolean Parser: http://www.informit.com/articles/article.aspx?p=1405547&seqNum=3
"""

class DataTreeWidgetHierarchy(QtGui.QTreeWidget):

    def __init__(self, view, parent = None):

        super(DataTreeWidgetHierarchy, self).__init__(parent)
        self.__m_view = view
        self.configTree(2)

    def configTree(self, col):

        self.setColumnCount(col)

        horHeaders = []
        horHeaders.append("Property")
        horHeaders.append("Value")
        self.setHeaderLabels(horHeaders)

        self.setAlternatingRowColors(True)
        self.setEditTriggers(QtGui.QAbstractItemView.EditKeyPressed)
        self.header().setMovable(False)
        self.header().setResizeMode(QtGui.QHeaderView.Stretch)

    def load(self, properties):

        props = properties.getProperties()
        rows = properties.count()

        columns = 2

        self.configTree(columns)

        """
            Tree widget items are used to hold rows of information for tree widgets.
        """

        for i, prop in enumerate(props):

            newItem = self.createNewRow(prop, self.invisibleRootItem()) # root QTreeWidgetItem

            self.setItemExpanded(newItem, True) #Warning: The QTreeWidgetItem must be added to the QTreeWidget before calling this function

    def clearAll(self):

        self.clear()

    def createNewRow(self, prop, parent):

        propertyName = prop.name
        newItem = QtGui.QTreeWidgetItem(parent) # Detail to generate a hierarchical tree, the parent must be passed in the constructor QTreeWidgetItem
        newItem.setFlags(newItem.flags() | QtCore.Qt.ItemIsEditable)
        newItem.setText(0, prop.name) # Property Name
        newItem.setData(0, QtCore.Qt.UserRole, QtCore.QVariant(propertyName)) # Property Name

        propertyValue = prop.value
        newWidget = 0

        if(prop.get_type() == EnumDataType.Integer):

            # Second Column
            newItem.setData(1, prop.get_type(), QtCore.QVariant(propertyValue)) # Property Value
            newWidget = QtGui.QSpinBox(self)
            newWidget.setValue(propertyValue)

        elif (prop.get_type() == EnumDataType.Double):

            # Second Column
            newItem.setData(1, prop.get_type(), QtCore.QVariant(propertyValue)) # Property Value
            newWidget = QtGui.QDoubleSpinBox(self)
            newWidget.setValue(propertyValue)

        elif (prop.get_type() == EnumDataType.String):

            # Second Column
            newItem.setData(1, prop.get_type(), QtCore.QVariant(propertyValue)) # Property Value
            newWidget = QtGui.QLineEdit(self)
            newWidget.setText(propertyValue)

        if(prop.has_child() == True):

            newChildItem = self.createNewRow(prop.get_child(), newItem) #add child

        if(newWidget != 0):

            self.setItemWidget(newItem, 1, newWidget) # Property Value

        return newItem