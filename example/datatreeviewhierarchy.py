import types
from PyQt4 import QtGui
from PyQt4 import QtCore

"""
    QTreeView no has a predefined model
    Fonte: http://stackoverflow.com/questions/27898718/multi-level-qtreeview
"""

class DataTreeViewHierarchy(QtGui.QTreeView):

    def __init__(self, view, parent = None):

        super(DataTreeViewHierarchy, self).__init__(parent)

        self.__m_view = view

        horHeaders = QtCore.QStringList()
        horHeaders.append("Property")
        horHeaders.append("Value")

        self.setAlternatingRowColors(True)
        self.setEditTriggers(QtGui.QAbstractItemView.EditKeyPressed)
        self.header().setMovable(False)
        self.header().setResizeMode(QtGui.QHeaderView.Stretch)

        model = QtGui.QStandardItemModel()
        model.setHorizontalHeaderLabels (horHeaders)

        tree = {'root': {
                    "1": ["A", "B", "C"],
                    "2": {
                        "2-1": ["G", "H", "I"],
                        "2-2": ["J", "K", "L"]},
                    "3": ["D", "E", "F"]}
        }

        self._populateTree(tree, model.invisibleRootItem(), model)

        self.setModel(model)

        for i in range(0, model.columnCount()):
            self.resizeColumnToContents(i)

    def _populateTree(self, children, parent, model):

        for child in sorted(children):

            child_item = QtGui.QStandardItem(child)

            parent.appendRow(child_item)

            if isinstance(children, types.DictType):

                self._populateTree(children[child], child_item, model)


    def clearAll(self):

        #self.clear()
        return

