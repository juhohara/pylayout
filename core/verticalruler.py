import sys
from PyQt4 import QtGui
from PyQt4 import QtCore

class VerticalRuler:

    def __init__(self):
        self.__zoom = 1.
        self.__m_matrix = QtGui.QTransform()
		
    def drawRuler(self, view, painter, scale) :

        m_blockSize = 10
        m_middleBlockSize = 5
        m_smallBlockSize = 1
        m_longLine = 7.
        m_mediumLine = 6.5
        m_smallLine = 4.5
        m_height = 20
        m_cornerSize = 20
        m_spacingLineText = 9.5
  
        brush = painter.brush()
        bhWhite = QtGui.QBrush(QtGui.QColor(255,255,255,255))
        bhGreyBack = QtGui.QBrush(QtGui.QColor(145,145,145,255))
        bhGreyBox = QtGui.QBrush(QtGui.QColor(180,180,180,255))

        pen = QtGui.QPen(QtGui.QColor(0,0,0,255))

        zoomFactor = 1. / scale #Keeps the appearance of the ruler to 100%

        ll = view.mapToScene(0, view.height())  
        ur = view.mapToScene(view.width(), 0)

        w = 210
        h = 297

        #Vertical Ruler
        rfV = QtCore.QRectF(QtCore.QPointF(ll.x(), ll.y()), QtCore.QPointF(ll.x() + m_height * zoomFactor, ur.y()))
        rfBackV = QtCore.QRectF(QtCore.QPointF(ll.x(), ll.y()), QtCore.QPointF(ll.x() + (m_cornerSize - 1.5) * zoomFactor, ur.y() - ((m_height) * zoomFactor)))
        rfPaperV = QtCore.QRectF(QtCore.QPointF(ll.x(), 0), QtCore.QPointF(ll.x() + (m_cornerSize - 1.5) * zoomFactor, h))
        rfLineV = QtCore.QLineF(QtCore.QPointF(ll.x() + m_cornerSize * zoomFactor, ll.y()), QtCore.QPointF(ll.x() + m_height * zoomFactor, ur.y() - (m_height * zoomFactor)))

        #Rect corner
        rfRectCorner = QtCore.QRectF(QtCore.QPointF(ll.x(), ur.y()), QtCore.QPointF(ll.x() + m_cornerSize * zoomFactor, ur.y() - m_height * zoomFactor))

        painter.save()
        painter.setPen(QtCore.Qt.NoPen)
  
        #Vertical Ruler
        painter.setBrush(bhGreyBox)
        painter.drawRect(rfV)

        painter.setBrush(bhGreyBack)
        painter.drawRect(rfBackV)

        painter.setBrush(bhWhite)
        painter.drawRect(rfPaperV)

        painter.setBrush(brush)
        painter.setPen(pen)

        painter.drawLine(rfLineV)

        ft = QtGui.QFont("Arial")
        ft.setPointSizeF(6)
        painter.setFont(ft)

        urx = rfBackV.topRight().x()
        ury = rfBackV.bottomLeft().y()
        lly = rfBackV.topRight().y()
  
        x = urx

        #Vertical Ruler Marks
        for i in range(int(lly), int(ury)):

            if i % m_blockSize == 0:

                box = QtCore.QLineF(QtCore.QPointF(x, i), QtCore.QPointF(x - m_longLine * zoomFactor, i))

                ss = str(i)

                qss = QtCore.QString(ss)

                wtxt, htxt = self.textBoundingBox(ss, -90)
                
                pTranslate = QtCore.QPointF(x - m_spacingLineText * zoomFactor, i)
      
                p1 = view.mapFromScene(pTranslate)
                p1.setY(p1.y() + wtxt/2.)

                painter.save()

                m = QtGui.QTransform()
                m.translate( p1.x(), p1.y() )
                m.rotate(-90)
                m.translate( -p1.x(), -p1.y() )

                painter.setMatrixEnabled(False)
                painter.setTransform(m)
                painter.setFont(ft)
                painter.drawText(p1, qss)

                painter.restore()
    
            elif i % m_middleBlockSize == 0:

                box = QtCore.QLineF(QtCore.QPointF(x, i), QtCore.QPointF(x - m_mediumLine * zoomFactor, i))

            elif i % m_smallBlockSize == 0:

                box = QtCore.QLineF(QtCore.QPointF(x, i), QtCore.QPointF(x - m_smallLine * zoomFactor, i))
    

            painter.drawLine(box)  

        painter.setBrush(bhGreyBox)
        painter.setPen(QtCore.Qt.NoPen)
        painter.drawRect(rfRectCorner)

        painter.restore()

    def textBoundingBox(self, txt, angle = 0):

        ax, ay = 0.,0. #alignment position
        qtx = QtCore.QString(txt)
        m_font = QtGui.QFont("Arial", 12)
        fm = QtGui.QFontMetrics(m_font)
        rec = QtCore.QRectF(fm.boundingRect(qtx))
        angle = 0

        transfInv, flag = self.__m_matrix.inverted()
        
        wrec = QtCore.QRectF(transfInv.mapRect(rec))

        ax = wrec.left();

        #reverse top-bottom
        ay = wrec.top();
        
        wrec.translate(-ax, -ay) #rotation point on text alignment
        polf = QtGui.QPolygonF(wrec)

        if(angle != 0.) :
        
            m = QtGui.QMatrix()
            m.rotate(-angle)
            polf = m.map(polf)


        p = QtCore.QPoint(0,0)
        wp = transfInv.map(QtCore.QPointF(p))
        polf.translate(wp.x(), wp.y()) #translate to entry point

        wtxt = polf.boundingRect().width()
        htxt = polf.boundingRect().height()
        
        return wtxt, htxt

    def matrix(self, m):
        self.__m_matrix = m
