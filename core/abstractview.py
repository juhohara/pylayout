import collections
from utils import EnumModes

class AbstractView(object):

    def __init__(self):
        self._m_visibleRulers = True
        self._m_currentZoom = 50
        self._m_defaultZoom = 50
        self._m_oldZoom = 50
        self._m_maxZoomLimit = 900
        self._m_minZoomLimit = 10

        self._m_zooms = {}
        self._m_zooms[10] = "10%"
        self._m_zooms[25] = "25%"
        self._m_zooms[33] = "33%"
        self._m_zooms[42] = "42%"
        self._m_zooms[50] = "50%"
        self._m_zooms[70] = "70%"
        self._m_zooms[100] = "100%"
        self._m_zooms[150] = "150%"
        self._m_zooms[200] = "200%"
        self._m_zooms[300] = "300%"
        self._m_zooms[400] = "400%"
        self._m_zooms[600] = "600%"
        self._m_zooms[700] = "700%"
        self._m_zooms[800] = "800%"
        self._m_zooms[900] = "900%"

        self._m_currentMode = EnumModes.NO_ACTION
        self._m_oldMode = self._m_currentMode

    def isVisibleRulers(self):
      return self._m_visibleRulers

    def setVisibleRulers(self, visible):
        self._m_visibleRulers = visible

    def addZoom(self, zoom, text):
        self._m_zooms[zoom] = text

    def removeZoom(self, zoom):
        if (zoom in self._m_zooms):
            del self._m_zooms[zoom]

    def clearZoomList(self):
        self._m_zooms.clear()

    def nextZoom(self):

        zoom = 0

        """Dictionary is not sorted, use import collections,
        collections.OrderedDict() """
        sorted_zooms = collections.OrderedDict(sorted(self._m_zooms.items()))

        if (self._m_currentZoom in self._m_zooms):

            currentIndex = sorted_zooms.keys().index(self._m_currentZoom)

            n = len(self._m_zooms)
            next = currentIndex+1

            if(next < n):
                zoom = sorted_zooms.keys()[next]
            else:
                zoom = self.findGreaterZoomThanCurrent()

        return zoom

    def previousZoom(self):

        zoom = 0

        """Dictionary is not sorted, use import collections,
        collections.OrderedDict() """
        sorted_zooms = collections.OrderedDict(sorted(self._m_zooms.items()))

        if (self._m_currentZoom in self._m_zooms):

            currentIndex = sorted_zooms.keys().index(self._m_currentZoom)

            n = len(self._m_zooms)
            previous = currentIndex-1

            if(previous >= 0):
                zoom = sorted_zooms.keys()[previous]
            else:
                zoom = self.findLessZoomThanCurrent()

        return zoom

    def setDefaultZoom(self, zoom):
        self._m_defaultZoom = zoom

    def getDefaultZoom(self):
        return self._m_defaultZoom

    def getCurrentZoom(self):
        return self._m_currentZoom

    def getOldZoom(self):
        return self._m_oldZoom

    def isLimitExceeded(self, newZoom):

        result = False
        #Zooming In
        if(newZoom > self._m_maxZoomLimit or newZoom < self._m_minZoomLimit):
          result = True

        return result

    def setMaxZoomLimit(self, zoom):
        self._m_maxZoomLimit = zoom

    def getMaxZoomLimit(self):
        return self._m_maxZoomLimit

    def setMinZoomLimit(self, zoom):
        self._m_minZoomLimit = zoom

    def getMinZoomLimit(self):
        return self._m_minZoomLimit

    def setCurrentZoom(self, zoom):

        if(self._m_currentZoom == zoom):
            return

        self._m_oldZoom = self._m_currentZoom
        self._m_currentZoom = zoom

    def getCurrentMode(self):
        return self._m_currentMode

    def getOldMode(self):
        return self._m_oldMode

    def setCurrentMode(self, mode):
        self._m_oldMode = self._m_currentMode
        self._m_currentMode = mode

    def findGreaterZoomThanCurrent(self):

        zoom = 0

        """Dictionary is not sorted, use import collections,
        collections.OrderedDict() """
        sorted_zooms = collections.OrderedDict(sorted(self._m_zooms.items()))

        count = len(self._m_zooms)

        for i in range(count):

            keyZoom = sorted_zooms.keys()[i]
            if(keyZoom > self._m_currentZoom):
                zoom = keyZoom

        return zoom

    def findLessZoomThanCurrent(self):

        zoom = 0

        """Dictionary is not sorted, use import collections,
        collections.OrderedDict() """
        sorted_zooms = collections.OrderedDict(sorted(self._m_zooms.items()))

        count = len(self._m_zooms)

        for i in range(count):

            keyZoom = sorted_zooms.keys()[i]
            if(keyZoom < self._m_currentZoom):
                zoom = keyZoom

        return zoom

