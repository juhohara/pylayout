from PyQt4 import QtCore
from box import Box

class WorldTransformer:

    def __init__(self):

      self._m_scaleX = 0.
      self._m_scaleY = 0.
      self._m_translateX = 0
      self._m_translateY = 0
      self._m_s1llx = 0.
      self._m_s1lly = 0.
      self._m_s1urx = 0.
      self._m_s1ury = 0.
      self._m_s1Width = 0.
      self._m_s1Height = 0.
      self._m_s2llx = 0.
      self._m_s2lly = 0.
      self._m_s2urx = 0.
      self._m_s2ury = 0.
      self._m_s2Width = 0.
      self._m_s2Height = 0.
      self._m_valid = False
      self._m_mirroring = True

    def __init__(self, s1llx, s1lly, s1urx, s1ury, s2width, s2height):

        system1Box = Box(s1llx, s1lly, s1urx, s1ury)
        system2Box = Box(0, 0, s2width, s2height)

        self.setTransformationParameters(system1Box, system2Box)

    def __init__(self,system1Box, system2Box ):

        self.setTransformationParameters(system1Box, system2Box)

    def setTransformationParameters(self, system1Box, system2Box):

        self._m_valid = True
        if not((system1Box.isValid() == False) or (system2Box.isValid() == False)):

            self._m_valid = False
            return

        self.initVariables(system1Box, system2Box)

        self._m_scaleX = self._m_s2Width / self._m_s1Width #map units per unit along x-direction
        self._m_scaleY = self._m_s2Height / self._m_s1Height #map units per unit along y-direction

        self._m_translateX = self._m_s2llx - self._m_s1llx * self._m_scaleX
        self._m_translateY = self._m_s2lly - self._m_s1lly * self._m_scaleY

    def system1Tosystem2(self, wx, wy):

        wx = (self._m_scaleX * wx) + self._m_translateX
        wy = (self._m_scaleY * wy) + self._m_translateY

        if(self._m_mirroring):

            dyCopy = wy
            wy = self._m_s2Height - 1 - dyCopy; #mirror

        return wx, wy

    def system1Tosystem2(self, wx, wy, dx, dy):

        dx = (self._m_scaleX * wx) + self._m_translateX
        dy = (self._m_scaleY * wy) + self._m_translateY

        if(self._m_mirroring):

            dyCopy = dy;
            dy = self._m_s2Height - 1 - dyCopy #mirror

        return wx, wy, dx, dy

    def system1Tosystem2(self, c1):

        x2 = 0.
        y2 = 0.

        x2, y2 = self.system1Tosystem2(c1.x(), c1.y())

        c2 = QtCore.QPointF(x2, y2)
        return c2

    def system2Tosystem1(self, dx, dy):

        dyCopy = dy

        if(self._m_mirroring):

            dy = self._m_s2Height - 1 - dyCopy #mirror


        dx = (dx - self._m_translateX) / self._m_scaleX
        dy = (dyCopy - self._m_translateY) / self._m_scaleY

        return dx, dy

    def system2Tosystem1(self, dx, dy, wx, wy):

        dyCopy = dy

        if(self._m_mirroring):
            dy = self._m_s2Height - 1 - dyCopy #mirror

        wx = (dx - self._m_translateX) / self._m_scaleX
        wy = (dyCopy - self._m_translateY) / self._m_scaleY

        return wx, wy

    def initVariables(self, system1Box, system2Box):

        self._m_scaleX = 0.
        self._m_scaleY = 0.

        self._m_translateX = 0.
        self._m_translateY = 0.

        self._m_s1llx = system1Box.get_x1()
        self._m_s1lly = system1Box.get_y1()
        self._m_s1urx = system1Box.get_x2()
        self._m_s1ury = system1Box.get_y2()
        self._m_s1Width = system1Box.getWidth()
        self._m_s1Height = system1Box.getHeight()

        self._m_s2llx = system2Box.get_x1()
        self._m_s2lly = system2Box.get_y1()
        self._m_s2urx = system2Box.get_x2()
        self._m_s2ury = system2Box.get_y2()
        self._m_s2Width = system2Box.getWidth()
        self._m_s2Height = system2Box.getHeight()

    def getScaleX(self):
        return self._m_scaleX

    def getScaleY(self):
        return self._m_scaleY

    def getTranslateX(self):
        return self._m_translateX

    def getTranslateY(self):
        return self._m_translateY

    def getS1llx(self):
        return self._m_s1llx

    def getS1lly(self):
        return self._m_s1lly

    def getS1urx(self):
        return self._m_s1urx

    def getS1ury(self):
        return self._m_s1ury

    def getS1Width(self):
        return self._m_s1Width

    def getS1Height(self):
        return self._m_s1Height

    def getS2llx(self):
        return self._m_s2llx

    def getS2lly(self):
        return self._m_s2lly

    def getS2urx(self):
        return self._m_s2urx

    def getS2ury(self):
        return self._m_s2ury

    def getS2Width(self):
        return self._m_s2Width

    def getS2Height(self):
        return self._m_s2Height

    def isValid(self):
        if((self._m_scaleX <= 0.) and (self._m_scaleY <= 0.)):
            return False
        return True

    def isMirroring(self):
      return self._m_mirroring

    def setMirroring(self, mirror):
        self._m_mirroring = mirror
