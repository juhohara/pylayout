from PyQt4 import QtGui
from PyQt4 import QtCore

class HorizontalRuler:

    def __init__(self):
        self.__zoom = 1.
        self.__m_matrix = QtGui.QTransform()
		
    def drawRuler(self, view, painter, scale) :

        m_blockSize = 10
        m_middleBlockSize = 5
        m_smallBlockSize = 1
        m_longLine = 7.
        m_mediumLine = 6.5
        m_smallLine = 4.5
        m_height = 20
        m_cornerSize = 20
        m_spacingLineText = 9.5
  
        brush = painter.brush()
        bhWhite = QtGui.QBrush(QtGui.QColor(255,255,255,255))
        bhGreyBack = QtGui.QBrush(QtGui.QColor(145,145,145,255))
        bhGreyBox = QtGui.QBrush(QtGui.QColor(180,180,180,255))

        pen = QtGui.QPen(QtGui.QColor(0,0,0,255))

        zoomFactor = 1. / scale #Keeps the appearance of the ruler to 100%
      
        ll = view.mapToScene(0, view.height())  
        ur = view.mapToScene(view.width(), 0)

        w = 210

        #Horizontal Ruler
        rfH = QtCore.QRectF(QtCore.QPointF(ll.x(), ur.y()), QtCore.QPointF(ur.x(), ur.y() - m_height * zoomFactor))
        rfBackH = QtCore.QRectF(QtCore.QPointF(ll.x() + (m_cornerSize * zoomFactor), ur.y()), QtCore.QPointF(ur.x(), ur.y() - (m_height - 1.5) * zoomFactor))
        rfPaperH = QtCore.QRectF(QtCore.QPointF(0, ur.y()), QtCore.QPointF(w, ur.y() - (m_height - 1.5) * zoomFactor))
        rfLineH = QtCore.QLineF(QtCore.QPointF(ll.x() + (m_cornerSize * zoomFactor), ur.y() - m_height * zoomFactor), QtCore.QPointF(ur.x(), ur.y() - m_height * zoomFactor))

        #Rect corner
        rfRectCorner = QtCore.QRectF(QtCore.QPointF(ll.x(), ur.y()), QtCore.QPointF(ll.x() + m_cornerSize * zoomFactor, ur.y() - m_height * zoomFactor))

        painter.save()
        painter.setPen(QtCore.Qt.NoPen)

        #Horizontal Ruler
        painter.setBrush(bhGreyBox)
        painter.drawRect(rfH)

        painter.setBrush(bhGreyBack)
        painter.drawRect(rfBackH)

        painter.setBrush(bhWhite)
        painter.drawRect(rfPaperH)

        painter.setBrush(brush)
        painter.setPen(pen)

        painter.drawLine(rfLineH)

        ft = QtGui.QFont("Arial")
        ft.setPointSizeF(6)
        painter.setFont(ft)

        llx = rfBackH.bottomLeft().x()
        lly = rfBackH.bottomLeft().y()
        urx = rfBackH.topRight().x()

        y = lly

        #Horizontal Ruler Marks
        for i in range(int(llx), int(urx)):
            
            if i % m_blockSize == 0:
                box = QtCore.QLineF(QtCore.QPointF(i, y), QtCore.QPointF(i, y + m_longLine * zoomFactor))
                
                ss = str(i)

                qss = QtCore.QString(ss)

                wtxt, htxt = self.textBoundingBox(ss)

                p = view.mapFromScene(QtCore.QPointF(i, y + m_spacingLineText * zoomFactor))
                p.setX(p.x() - wtxt/2.)
                
                #Keeps the size of the text.(Aspect)
                painter.setMatrixEnabled(False)
                painter.drawText(p, qss)
                painter.setMatrixEnabled(True)
    
            elif i % m_middleBlockSize == 0:
                box = QtCore.QLineF(QtCore.QPointF(i, y), QtCore.QPointF(i, y + m_mediumLine * zoomFactor))
            elif i % m_smallBlockSize == 0:
                box = QtCore.QLineF(QtCore.QPointF(i, y), QtCore.QPointF(i, y + m_smallLine * zoomFactor))
    
            painter.drawLine(box)
  

        painter.setBrush(bhGreyBox)
        painter.setPen(QtCore.Qt.NoPen)
        painter.drawRect(rfRectCorner)

        painter.restore()

    def textBoundingBox(self, txt):

        ax, ay = 0.,0. #alignment position
        qtx = QtCore.QString(txt)
        m_font = QtGui.QFont("Arial", 12)
        fm = QtGui.QFontMetrics(m_font)
        rec = QtCore.QRectF(fm.boundingRect(qtx))
        angle = 0

        transfInv, flag = self.__m_matrix.inverted()
        
        wrec = QtCore.QRectF(transfInv.mapRect(rec))

        ax = wrec.left()

        #reverse top-bottom
        ay = wrec.top()
        
        wrec.translate(-ax, -ay) #rotation point on text alignment
        polf = QtGui.QPolygonF(wrec)

        if(angle != 0.) :
        
            m = QtGui.QMatrix()
            m.rotate(-angle)
            polf = m.map(polf)


        p = QtCore.QPoint(0,0)
        wp = transfInv.map(QtCore.QPointF(p))
        polf.translate(wp.x(), wp.y()) #translate to entry point

        wtxt = polf.boundingRect().width()
        htxt = polf.boundingRect().height()
        
        return wtxt, htxt

    def matrix(self, m):
        self.__m_matrix = m
