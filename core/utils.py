from worldtransformer import WorldTransformer

class Utils:

    """
        Primitive values
        safe_cast('tst', int) # will return None
        safe_cast('tst', int, 0) # will return 0
    """
    @staticmethod
    def safe_primitive_cast(val, to_type, default=None):
        try:
            return to_type(val)
        except ValueError:
            return default

    @staticmethod
    def mm2pixel(mm):

        devDpi = 96
        px = float(mm * devDpi) / 25.4
        return px

    @staticmethod
    def pixel2mm(pixel):

        devDpi = 96
        mm = float(pixel) / float(devDpi) * 25.4
        return mm

    @staticmethod
    def getTransform(self, boxSystem1, boxSystem2):

        transf = WorldTransformer() #World Transformer.

        if(boxSystem1.isValid() == False):
            return transf

        if(boxSystem2.isValid() == False):
            return transf

        #Adjust internal renderer transformer
        transf.setTransformationParameters(boxSystem1, boxSystem2)

        return transf

class EnumModes:
    RESIZE_ACTION, NO_ACTION, MOVE_ACTION = range(3)
