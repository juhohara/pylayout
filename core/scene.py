from PyQt4 import QtGui
from PyQt4 import QtCore
from box import Box

class Scene(QtGui.QGraphicsScene):

    def __init__(self, parent = None):
        
        QtGui.QGraphicsScene.__init__(self, parent)

        self.__box = Box(0,0,0,0)
        self.__zoomFactor = 1.
        self.__scale = 1.
        self.__dpiX = 96
        self.__dpiY = 96
        self.__modeEdition = False
        self.__currentItemEdition = 0

        self.__m_backgroundColor = QtGui.QColor(109,109,109)
        self.setBackgroundBrush(QtGui.QBrush(self.__m_backgroundColor))

    def config(self, wMM, hMM) :

        self.calculateWindow(wMM, hMM)

        w = self.__box.getWidth()
        h = self.__box.getHeight()

        self.calculateWindow(w,h)

        self.calculateMatrixViewScene()

    def handlePreview(self):

        printer = QtGui.QPrinter(QtGui.QPrinter.HighResolution)
        printer.setPageSize(QtGui.QPrinter.A4)
        printer.setOrientation( QtGui.QPrinter.Portrait )
        printer.pageRect(QtGui.QPrinter.Millimeter)
        dialog = QtGui.QPrintPreviewDialog(printer)
        dialog.paintRequested.connect(self.handlePaintRequest)
        dialog.exec_()

    def handlePaintRequest(self, printer):
        
        painter = QtGui.QPainter(printer)
        pxTargetRect = QtCore.QRectF(0, 0, painter.device().width(), painter.device().height())

        #Convert Paper Size world to screen coordinate. Uses dpi printer.
        paperPixelBox = printer.paperSize(QtGui.QPrinter.DevicePixel)

        #Mirroring Y-Axis
        painter.translate( paperPixelBox.width() / 2, paperPixelBox.height() / 2 )
        painter.scale( 1, -1 )
        painter.translate( -(paperPixelBox.width() / 2), -(paperPixelBox.height() / 2) )

        #Without mirroring the scene is drawn upside down,
        #since that uses cartesian system
        self.render(painter, pxTargetRect, QtCore.QRectF(0, 0, 210, 297))

    def calculateMatrixViewScene(self):
        
        llx = self.__box.x1
        lly = self.__box.y1
        urx = self.__box.x2
        ury = self.__box.y2

        newZoomFactor = (self.__dpiX / 25.4) 

        #mm (inversao do y)
        self.__m_matrix = QtGui.QTransform().scale(newZoomFactor, -newZoomFactor).translate(-llx, -ury)
        #Coordenadas de mundo - mm
        self.setSceneRect(QtCore.QRectF(QtCore.QPointF(llx, lly), QtCore.QPointF(urx, ury)))
        
    def calculateWindow(self, wMM, hMM):

        ppSizeWMM = 210
        ppSizeHMM = 297
        
        x1 = 0
        y1 = 0
        x2 = 0
        y2 = 0
        
        paddingX = 0
        paddingY = 0

        #X-Axis
        minX = min(ppSizeWMM, wMM)
        maxX = max(ppSizeWMM, wMM)

        paddingX = (maxX - minX) / 2
        x1 = - paddingX
        x2 = maxX - paddingX

        #Y-Axis
        minY = min(ppSizeHMM, hMM)
        maxY = max(ppSizeHMM, hMM)

        paddingY = (maxY - minY) / 2
        y1 = - paddingY
        y2 = maxY - paddingY

        self.__box = Box(x1, y1, x2, y2)

    def drawForeground (self, painter, rect) :

        if(self.is_modeEdition() == True):

            painter.save()

            if not self.__currentItemEdition:
                return

            if(self.__currentItemEdition.is_modeEdition() == True):

                rec = self.__currentItemEdition.sceneBoundingRect ()

                outerPath = QtGui.QPainterPath()
                outerPath.setFillRule(QtCore.Qt.WindingFill)
                outerPath.addRect(rect) # rectangle outside

                innerPath = QtGui.QPainterPath()
                innerPath.addRect(rec) # rectangle inside
                fillPath = outerPath.subtracted(innerPath)

                #config painter
                backgroundColor = QtGui.QColor(0,0,0,80)
                painter.setRenderHint(QtGui.QPainter.Antialiasing)
                painter.fillPath(fillPath, backgroundColor)

                # paint the outlines
                # QPainterPath::simplified(): this converts the set of layered shapes
                # into one QPainterPath which has no intersections
                contourColor = QtGui.QColor(178,34,34)
                penOuterPath = QtGui.QPen(QtCore.Qt.NoPen)
                painter.strokePath(outerPath.simplified(), penOuterPath)
                painter.strokePath(innerPath, QtGui.QPen(contourColor, 2))

            painter.restore()

        super(Scene, self).drawForeground(painter, rect)

    def keyPressEvent(self, event):

        if event.key() == QtCore.Qt.Key_Escape:
            print "Hi! I'm Scene. I'm in keyPressEvent."
            print "--------------------------------"
            self.__leaveEditionMode() #Edition off

            if(isinstance(self.__currentItemEdition, TextItem)):
                self.getView().closeTextToolbar()

        super(Scene, self).keyPressEvent(event)

    def mouseDoubleClickEvent (self, event):
        super(Scene, self).mouseDoubleClickEvent(event)

        print "Hi! I'm Scene. I'm in mouseDoubleClickEvent."
        print "--------------------------------"

        self.__enterEditionMode()

        if(isinstance(self.__currentItemEdition, TextItem)):
            self.getView().showTextToolbar()
        else:
            self.getView().closeTextToolbar()

    def mousePressEvent (self, event):
        super(Scene, self).mousePressEvent(event)

    def mouseMoveEvent (self, event):

        # The mouse move should only be passed on to the item in the current edition
        if(self.is_modeEdition() == False):
            super(Scene, self).mouseMoveEvent(event)

    def mouseReleaseEvent (self, event):
        super(Scene, self).mouseReleaseEvent(event)

    def getView(self):

        cot = len(self.views())
        if(cot > 0):
            return self.views().pop(0)

        return 0

    def __leaveEditionMode(self):

        if not self.__currentItemEdition:
            return

        print "Hi! I'm Scene. Edition mode off"
        print "--------------------------------"

        self.__currentItemEdition.set_modeEdition(False)
        self.__currentItemEdition.hello()
        self.__modeEdition = False
        self.update()

    def __enterEditionMode(self):

        if not self.mouseGrabberItem():
            return

        print "Hi! I'm Scene. Edition mode on"
        print "--------------------------------"

        if(self.__currentItemEdition):
            self.__currentItemEdition.set_modeEdition(False)

        self.__currentItemEdition = self.mouseGrabberItem()
        self.__currentItemEdition.set_modeEdition(True)
        self.__currentItemEdition.hello()
        self.__modeEdition = True
        self.update()

    def get_matrix(self):
        return self.__m_matrix
  
    def set_matrix(self, v):
        self.__m_matrix = v

    def get_box(self):
        return self.__box
  
    def set_box(self, v):
        self.__box = v

    def get_zoomFactor(self):
        return self.__zoomFactor
  
    def set_zoomFactor(self, v):
        self.__zoomFactor = v

    def is_modeEdition(self):
        return self.__modeEdition

    def set_modeEdition(self, v):
        self.__modeEdition = v

    # ao ser usado, chama o get ou set
    matrix = property(fget=get_matrix, fset=set_matrix)

    # ao ser usado, chama o get ou set
    box = property(fget=get_box, fset=set_box)

    # ao ser usado, chama o get ou set
    zoomFactor = property(fget=get_zoomFactor, fset=set_zoomFactor)


