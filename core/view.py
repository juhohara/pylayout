from PyQt4 import QtGui
from PyQt4 import QtCore
from PyQt4.QtCore import Qt

from scene import Scene
from horizontalruler import HorizontalRuler
from verticalruler import VerticalRuler
from core.datatree.datatree import DataTree
from outside.toolbaroutside import ToolbarOutside
from tools.viewzoomarea import ViewZoomArea
from tools.viewzoomclick import ViewZoomClick
from item.textitem import TextItem
from item.rectangleitem import RectangleItem
from item.mapitem import MapItem
from item.griditem import GridItem
from item.mapcompositionitem import MapCompositionItem
from abstractview import AbstractView


class View(QtGui.QGraphicsView, AbstractView):

    def __init__(self):
        
        QtGui.QGraphicsView.__init__(self)
        AbstractView.__init__(self)
        self.scene = Scene(self)
        self.setScene(self.scene)

        self.__horizontalRuler = HorizontalRuler()
        self.__verticalRuler = VerticalRuler()

        self.__dock = QtGui.QDockWidget(self.viewport().parent())
        self.__dock.setFloating(True)
        textToolbar = ToolbarOutside(self.viewport().parent())
        self.__dock.setWidget(textToolbar)
        self.__dock.setVisible(False)

        self.__dpiX = 96
        self.__dpiY = 96

        self.setDragMode(QtGui.QGraphicsView.RubberBandDrag)

        self.__zoomAreaTool = ViewZoomArea(self, QtGui.QCursor(QtCore.Qt.CrossCursor))
        self.__zoomClickTool = ViewZoomClick(self, QtGui.QCursor(QtCore.Qt.CrossCursor))
		
    def mousePressEvent(self, event):            
        return QtGui.QGraphicsView.mousePressEvent(self, event)

    def mouseReleaseEvent(self, event):

        selectedItems = self.scene.selectedItems()

        total = len(selectedItems)
        if total > 0:
            self.__dataTree.clearAll()
            item = selectedItems[0]
            self.__dataTree.load(item.getProperties())
        else:
            self.__dataTree.clearAll()

        return QtGui.QGraphicsView.mouseReleaseEvent(self, event)

    def wheelEvent(self, event):
        """In edit mode not apply the zoom,
        but part of the current item characteristics for this event.
        Example: Zoom on the map."""
        if (self.scene.is_modeEdition()):
            QtGui.QGraphicsView.wheelEvent(self,event)
            return

        mode = self.viewportUpdateMode()
        self.setViewportUpdateMode(QtGui.QGraphicsView.FullViewportUpdate)

        zoom = 0

        #Zoom in / Zoom Out with mouse scroll
        if (event.delta() > 0):
            #Zooming In
            zoom = self.nextZoom()

        else:
            zoom = self.previousZoom()

        self.setZoom(zoom)

        if (self.isLimitExceeded(zoom) == False):
            QtGui.QGraphicsView.wheelEvent(self,event)

        self.setViewportUpdateMode(mode)

    def keyPressEvent(self, event):

        if((event.modifiers() == Qt.ControlModifier) & (event.key() == Qt.Key_P)): #Print
            self.resetDefaultConfig()
            # No update Widget while print is running
            self.setUpdatesEnabled(False)
            self.scene.is_drawRuler = False
            self.scene.handlePreview()
            self.scene.is_drawRuler = True
            self.setUpdatesEnabled(True)

        if((event.modifiers() == Qt.AltModifier) & (event.key() == Qt.Key_P)): #Pan
            #Use ScrollHand Drag Mode to enable Panning
            self.resetDefaultConfig()
            #The entire viewport is redrawn to avoid traces
            self.setViewportUpdateMode(QtGui.QGraphicsView.FullViewportUpdate)
            self.setInteractive(False)
            self.setDragMode(QtGui.QGraphicsView.ScrollHandDrag)

        if((event.modifiers() == Qt.AltModifier) & (event.key() == Qt.Key_R)): #Recompose
            self.resetView()
            self.resetDefaultConfig()

        if((event.modifiers() == Qt.AltModifier) & (event.key() == Qt.Key_A)): #Active ZoomArea Tool
            self.resetDefaultConfig()
            self.setInteractive(False)
            self.viewport().installEventFilter(self.__zoomAreaTool)

        if((event.modifiers() == Qt.AltModifier) & (event.key() == Qt.Key_O)): #Active ZoomClick Tool
            self.resetDefaultConfig()
            self.setInteractive(False)
            self.viewport().installEventFilter(self.__zoomClickTool)

        if((event.modifiers() == Qt.AltModifier) & (event.key() == Qt.Key_Z)): #Reset to default config
            self.resetDefaultConfig()

        return QtGui.QGraphicsView.keyPressEvent(self, event)

    def drawForeground (self, painter, rect) :

        super(View, self).drawForeground(painter, rect)

        if(self._m_visibleRulers == False):
            return

        self.__horizontalRuler.matrix(self.scene.get_matrix())
        self.__verticalRuler.matrix(self.scene.get_matrix())

        currentScale = self.transform().m11()

        self.__horizontalRuler.drawRuler(self, painter, currentScale)
        self.__verticalRuler.drawRuler(self, painter, currentScale)

    def config(self) :

        sw = self.viewport().widthMM()
        sh = self.viewport().heightMM()

        self.scene.config(sw, sh)

        matrix = self.scene.matrix
        
        self.setTransform(matrix)

        self.scene.scaleView = self.transform().m11()

        box = self.scene.box

        self.centerOn(QtCore.QPointF(box.x1, box.y2))

        zoom = self.getDefaultZoom()
        newScale = zoom / 100.
        self.scale(newScale, newScale) #Initial zoom out
        self.setZoom(zoom)
        
        self.createPaper()
        self.createText()
        self.createRect()
        self.createTwoRect()
        self.createMapGrid()
        self.createMap()

        self.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)

    def createPaper(self):
        
        rct = QtCore.QRectF(0, 0, 210, 297)
        color = QtGui.QColor(255,255,255)
        br = QtGui.QBrush(QtGui.QBrush(color))
        
        item = self.scene.addRect(rct, QtGui.QPen(), br)
        item.setPos(0,0)

    def createText(self):
        rct = QtCore.QRectF(0, 0, 210, 297)
        color = QtGui.QColor(0,255,0)
        ft = QtGui.QFont("Arial", 12)

        item = TextItem()
        self.scene.addItem(item)
        item.setFont(ft)
        item.setFlag(QtGui.QGraphicsItem.ItemIsMovable, True)
        item.setFlag(QtGui.QGraphicsItem.ItemIsSelectable, True)
        transf, flag = self.scene.matrix.inverted()
        item.setTransform(transf)
        item.setPos(0,0)

    def createRect(self):

        item = RectangleItem()
        self.scene.addItem(item)
        item.setPos(50,70)

    def createTwoRect(self):

        item = RectangleItem()
        item.set_width(70)
        item.set_height(40)
        item.set_backgroundColor(QtGui.QColor(0,255,0))
        self.scene.addItem(item)
        item.setPos(20,50)

    def createMapGrid(self):

        grid = GridItem()
        grid.set_width(70)
        grid.set_height(50)
        self.scene.addItem(grid)
        grid.setPos(70,80)

        map = MapItem()
        map.set_width(120)
        map.set_height(120)
        self.scene.addItem(map)
        map.setPos(70,90)

        group = MapCompositionItem()
        self.scene.addItem(group)
        group.setPos(70,90)

        map.attach(grid)

        group.addToGroup(map)
        group.addToGroup(grid)

        group.adjustPos()

    def createMap(self):

        map = MapItem()
        map.set_width(120)
        map.set_height(120)
        self.scene.addItem(map)
        map.setPos(20,90)


    def setZoom(self,newZoom):
        #clears the foreground pixmap
        currentZoom = self.getCurrentZoom()

        if(newZoom == currentZoom):
            return

        if(self.isLimitExceeded(newZoom) == True):
            return

        currentHorizontalScale = self.viewportTransform().m11()
        currentVerticalScale = self.viewportTransform().m22()

        origHorizontalScale = self.scene.get_matrix().m11()
        origVerticalScale = self.scene.get_matrix().m22()

        hZoomScale = origHorizontalScale / (100./newZoom)
        vZoomScale = origVerticalScale / (100./newZoom)

        zoomScaleFactorH = hZoomScale / currentHorizontalScale
        zoomScaleFactorV = vZoomScale / currentVerticalScale

        if((zoomScaleFactorH > 0) and (zoomScaleFactorV > 0)):
            self.setCurrentZoom(newZoom)
            self.applyScale(zoomScaleFactorH, zoomScaleFactorV)
            self.update()

    def applyScale(self, horizontalScale, verticalScale):

        if((horizontalScale <= 0)or(verticalScale <= 0)):
            return

        self.scale(horizontalScale, verticalScale)

    def resetDefaultConfig(self):
        self.setDragMode(QtGui.QGraphicsView.RubberBandDrag)
        #Whole view not interactive while in ScrollHandDrag Mode
        self.setInteractive(True)
        self.setViewportUpdateMode(QtGui.QGraphicsView.MinimalViewportUpdate)
        self.setCursor(QtCore.Qt.ArrowCursor)
        self.viewport().removeEventFilter(self.__zoomAreaTool)
        self.viewport().removeEventFilter(self.__zoomClickTool)

    def resetView(self):
        matrix = self.scene.matrix
        self.setTransform(matrix)
        box = self.scene.box
        self.scene.scaleView = self.transform().m11()
        self.centerOn(QtCore.QPointF(box.x1, box.y2))

    def showTextToolbar(self):
        self.__dock.show()

    def closeTextToolbar(self):
        self.__dock.close()

    def get_factor(self):
        return self.__factor

    def is_drawRuler(self):
        return self.__isDrawRuler

    def set_drawRuler(self, v):
        self.__isDrawRuler = v

    def createDataTree(self):

        return self.createDataTreeWidgetWithDelegate()

    def createDataTreeWidgetWithDelegate(self):

        self.__dockTable = QtGui.QDockWidget(self.viewport().parent())
        self.__dockTable.setFloating(True)

        self.__dataTree = DataTree(self, None, self.viewport().parent())
        self.__dataTree.propertyChanged.connect(self._onPropertyChange)

        self.__dockTable.setFeatures(QtGui.QDockWidget.DockWidgetMovable)
        self.__dockTable.setAllowedAreas(Qt.LeftDockWidgetArea|Qt.RightDockWidgetArea)

        self.__dockTable.setWidget(self.__dataTree)
        self.__dockTable.setVisible(True)

        return self.__dockTable

    def _onPropertyChange(self, prop):

        selectedItems = self.scene.selectedItems()

        total = len(selectedItems)
        item = selectedItems[0]
        if(item):

            item.updateProperty(prop)
            print "View::_onPropertyChange..."

    # ao ser usado, chama o get ou set
    isDrawRuler = property(fget=is_drawRuler, fset=set_drawRuler)