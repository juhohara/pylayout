import sys
from PyQt4 import QtGui
from PyQt4 import QtCore
from view import View
from outside.toolbaroutside import ToolbarOutside

class Main(QtGui.QMainWindow):

    def __init__(self, parent = None):
        QtGui.QMainWindow.__init__(self,parent)
        #Dois underline significa que o atributo/metodo e privado
        self.__m_view = QtGui.QGraphicsView()
        self.initUI()

    def initUI(self):

        self.setWindowTitle("PyQt Graphics View - Print")

    def create(self):
        
        vLayout = QtGui.QVBoxLayout()
        vLayout.addWidget(self.__m_view)

        m_dockLayoutDisplay = QtGui.QDockWidget()
        m_dockLayoutDisplay.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
        m_groupBox = QtGui.QGroupBox(m_dockLayoutDisplay)
        m_groupBox.setLayout(vLayout)
        
        m_dockLayoutDisplay.setWidget(m_groupBox)
        m_dockLayoutDisplay.setParent(self)
        
        self.setCentralWidget(m_dockLayoutDisplay)
        m_dockLayoutDisplay.setVisible(True)

        toolbarOutside = ToolbarOutside(self.__m_view)
        self.addToolBar(toolbarOutside)

        #dockDataTable = self.__m_view.createDataTable()
        #self.addDockWidget(QtCore.Qt.LeftDockWidgetArea, dockDataTable)

        dockDataTree = self.__m_view.createDataTree()
        self.addDockWidget(QtCore.Qt.LeftDockWidgetArea, dockDataTree)
 
    def get_view(self):
        return self.__m_view
  
    def set_view(self, v):
        self.__m_view = v

    # ao ser usado, chama o get ou set
    view = property(fget=get_view, fset=set_view)

def main():

    print "Main - Start"

    app = QtGui.QApplication(sys.argv)

    size = QtGui.QApplication.desktop().screen().rect()
    
    main = Main()

    # x and y coordinates on the screen, width, height
    main.setGeometry(0, 0, size.width(), size.height())

    vw = View()
    vw.move( size.center() - vw.rect().center() )

    main.view = vw
    main.create()

    vw.config()
    vw.show()
    main.showMaximized()

    sys.exit(app.exec_())

if __name__ == "__main__":
    main()
