class Properties(object):

    def __init__(self, parent = None):
        self.__m_parent = parent
        self.__m_list = []

    def get_parent(self):
        return self.__m_parent
  
    def set_parent(self, parent):
        self.__m_parent = parent

    def addProperty(self, property):
        self.__m_list.append(property)

    def count(self):
        return len(self.__m_list)

    def getProperties(self):
        return self.__m_list

    def getProperty(self, name):

        count = len(self.__m_list)


        for i in range(0, count):

            if(self.__m_list[i].get_name() == name):

                return self.__m_list[i]

        return 0

    def updateProperty(self, prop):

        count = len(self.__m_list)
        for i in range(0, count):

            if(self.__m_list[i] == prop.get_name()):

                prop.set_value(prop.get_value())
                return True

        return False

    # ao ser usado, chama o get ou set
    parent = property(fget=get_parent, fset=set_parent)

