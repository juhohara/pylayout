class Property(object):

    def __init__(self, name, value, type, parent, visible = True):
        self.__m_name = name
        self.__m_parent = parent
        self.__m_value = value
        self.__m_type = type
        self.__m_child = 0
        self.__m_visible = visible

    def get_name(self):
        return self.__m_name
  
    def set_name(self, name):
        self.__m_name = name

    def get_parent(self):
        return self.__m_parent
  
    def set_parent(self, parent):
        self.__m_parent = parent

    def get_value(self):
        return self.__m_value

    def set_value(self, value):
        self.__m_value = value

    def get_type(self):
        return self.__m_type

    def set_child(self, property):
        self.__m_child = property

    def get_child(self):
        return self.__m_child

    def has_child(self):

        if(self.__m_child == 0):
            return False

        return True

    def isVisible(self):

        return self.__m_visible

    # ao ser usado, chama o get ou set
    name = property(fget=get_name, fset=set_name)
    # ao ser usado, chama o get ou set
    parent = property(fget=get_parent, fset=set_parent)
    # ao ser usado, chama o get ou set
    value = property(fget=get_value, fset=set_value)

