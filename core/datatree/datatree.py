from PyQt4 import QtGui
from PyQt4 import QtCore
from core.propertyeditor.propertydelegate import PropertyDelegate
from core.propertyeditor.paintcustomdatatype import PaintCustomDataType
from datatreeitemfactory import DataTreeItemFactory

"""
    This class contains the following signals:
        - propertyChanged()

    This parent class (QtGui.QTreeWidget) contains the following signals:
        - itemChanged: can't use because the value of the parent has not been updated yet. Ex.: Envelope data type.
"""


class DataTree(QtGui.QTreeWidget):

    # Create our own signal
    propertyChanged = QtCore.pyqtSignal(object)

    def __init__(self, view, delegate = None, parent = None):

        super(DataTree, self).__init__(parent)
        self.__m_view = view
        self._configTree(2)

        newDelegate = delegate

        if(delegate == None):
            paintCustom = PaintCustomDataType()
            newDelegate = PropertyDelegate(paintCustom, self)
            self.setItemDelegateForColumn(1, newDelegate) # Add new delegate to second column
        else:
            self.setItemDelegateForColumn(1, delegate) # Add new delegate to second column

        # connect signal/slot
        newDelegate.dataEditorChanged.connect(self.onDataEditorChanged) #signal/slot

    def _configTree(self, col):

        self.setColumnCount(col)

        horHeaders = []
        horHeaders.append("Property")
        horHeaders.append("Value")
        self.setHeaderLabels(horHeaders)

        self.setAutoFillBackground(True)
        self.setAlternatingRowColors(True)
        self.header().setMovable(False)
        self.header().setResizeMode(QtGui.QHeaderView.Stretch)

        self.setEditTriggers(QtGui.QAbstractItemView.CurrentChanged) # // Editing start whenever current item changes.

    def load(self, properties):

        props = properties.getProperties()
        rows = properties.count()

        columns = 2

        self._configTree(columns)

        """
            Tree widget items are used to hold rows of information for tree widgets.
        """

        for i, prop in enumerate(props):

            if(prop.isVisible()):

                newItem = self._createNewRow(prop, self.invisibleRootItem()) # root QTreeWidgetItem

                self.setItemExpanded(newItem, True) #Warning: The QTreeWidgetItem must be added to the QTreeWidget before calling this function

    def clearAll(self):

        self.clear()

    def _createNewRow(self, prop, parent):

        dict = {'property': prop, 'parent': parent}
        newItem = DataTreeItemFactory.build(dict, parent) # Detail to generate a hierarchical tree, the parent must be passed in the constructor QTreeWidgetItem

        propertyName = prop.name
        newItem.setFlags(newItem.flags() | QtCore.Qt.ItemIsEditable)
        newItem.setText(0, propertyName) # Property Name
        newItem.setData(0, QtCore.Qt.UserRole, QtCore.QVariant(propertyName)) # Property Name

        propertyValue = prop.value

        # Second Column
        #newItem.setText(1, str(propertyValue)) # Property Value
        newItem.setData(1, prop.get_type(), QtCore.QVariant(propertyValue)) # Property Value

        if(prop.has_child() == True):

            newChildItem = self._createNewRow(prop.get_child(), newItem) #add child

        return newItem

    def onDataEditorChanged(self, prop, row, column):

        print "DataTree::onDataEditorChanged::before..."

        item = self.currentItem()

        topParent = self._findTopParent(item.parent(), item.parent())
        if(topParent):

            prop = topParent.getProperty()
            self.propertyChanged.emit(prop)

            print "DataTree::onDataEditorChanged::after..."

    def _findTopParent(self, topParent, root):

        if(root):
            if(root != self.invisibleRootItem()):
                topParent = root
                if(root.parent()):
                    self._findTopParent(topParent, root.parent())
                else:
                    return topParent
            else:
                return topParent
        else:
            return topParent
