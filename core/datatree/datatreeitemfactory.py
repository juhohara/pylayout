from PyQt4 import QtCore
from core.enumdatatype import EnumDataType
from envelopedatatreeitem import EnvelopeDataTreeItem
from datatreeitem import DataTreeItem
from core.property.property import Property


class DataTreeItemFactory(object):

    # Create based on enum type:
    def build(dictParams, parent = None):

        prop = dictParams['property']
        parent = dictParams['parent']

        if(prop.get_type() == EnumDataType.Envelope):

            return DataTreeItemFactory.createEnvelopeDataTreeItem(prop, parent)

        return DataTreeItem(prop, parent)

    @staticmethod
    def createEnvelopeDataTreeItem(prop, parent):

        envelopeDataTreeItem = EnvelopeDataTreeItem(prop, parent)
        rect = prop.value

        firstChildName = "x1"
        childValue = rect.x()
        x1_child = DataTreeItemFactory.createDataTreeItemChild(firstChildName, childValue, envelopeDataTreeItem)

        secondChildName = "y1"
        childValue = rect.y()
        y1_child = DataTreeItemFactory.createDataTreeItemChild(secondChildName, childValue, envelopeDataTreeItem)

        thirdChildName = "x2"
        childValue = rect.bottomRight().x()
        x2_child = DataTreeItemFactory.createDataTreeItemChild(thirdChildName, childValue, envelopeDataTreeItem)

        fourthChildName = "y2"
        childValue = rect.bottomRight().y()
        y2_child = DataTreeItemFactory.createDataTreeItemChild(fourthChildName, childValue, envelopeDataTreeItem)

        return envelopeDataTreeItem

    @staticmethod
    def createDataTreeItemChild(name, value, parent):

        type = EnumDataType.Double
        prop = Property(name, value, type, 0)

        newItem = DataTreeItem(prop, parent) # Detail to generate a hierarchical tree, the parent must be passed in the constructor QTreeWidgetItem
        newItem.setFlags(newItem.flags() | QtCore.Qt.ItemIsEditable)
        newItem.setText(0, name) # Property Name
        newItem.setData(0, QtCore.Qt.UserRole, QtCore.QVariant(name)) # Property Name

        # Second Column
        newItem.setData(1, type, QtCore.QVariant(value)) # Property Value

        return newItem

    build = staticmethod(build)

