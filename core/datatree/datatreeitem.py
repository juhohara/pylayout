from PyQt4 import QtGui
from PyQt4 import QtCore

"""
    Obs.: QTreeWidgetItem, setData() calls setText() and vice versa.
"""


class DataTreeItem(QtGui.QTreeWidgetItem):

    def __init__(self, prop, parent = None):

        super(DataTreeItem, self).__init__(parent, prop.get_type())
        self._m_datatype = prop.get_type()
        self._m_property = prop

    """
        Reimplement method from QTreeWidgetItem
    """
    def setData(self, column, role, value):

        print "DataTreeItem::setData::before..."

        QtGui.QTreeWidgetItem.setData(self, column, role, value)

        if(column != 0):

            self._m_datatype = role
            name = self.data(0, QtCore.Qt.UserRole) # get name of the property

            if(self.parent()):

                self.parent().refresh(column, role, name.toString(), value, self) # refresh parent

        print "DataTreeItem::setData::after..."

    def getDataType(self):

        return self._m_datatype

    def getProperty(self):

        return self._m_property

    def setPropertyValue(self, variant):
        pass

    """
        Method to update the value of the property of its parent.
    """

    def refresh(self, column, type, name, value, child):

        print "DataTreeItem::refreshParent" + " " + str(column) + " " + str(name) + " " + str(value)

    def refreshChild(self, column, type, name, value):

        count = self.childCount()
        for i in range(0, count):

            itemChild = self.child(i)
            if(itemChild.data(0, QtCore.Qt.UserRole) == name):

                self._setChildData(itemChild, column, type, value)
                break

        print "DataTreeItem::refreshChild" + " " + str(column) + " " + str(name) + " " + str(value)

    def _setChildData(self, child, column, type, value):

        #child.setText(1, str(value)) # Property Value
        child.setData(column, type, value) # Property Value
        prop = child.getProperty() # How update child property (toSomething())?

