from PyQt4 import QtCore
from datatreeitem import DataTreeItem

class EnvelopeDataTreeItem(DataTreeItem):

    def __init__(self, type, parent = None):

        super(EnvelopeDataTreeItem, self).__init__(type, parent)
        self._m_x1_name = "x1"
        self._m_y1_name = "y1"
        self._m_x2_name = "x2"
        self._m_y2_name = "y2"

    """
        Reimplement method from QTreeWidgetItem
    """
    def setData(self, column, role, value):

        if(column == 0):
            DataTreeItem.setData(self, column, role, value)
            return

        DataTreeItem.setData(self, column, role, value)

    """
        Reimplement method from DataTreeItem
    """
    def refresh(self, column, type, name, value, child):

        if(column == 0):
            return;

        doubleValue, valid = value.toDouble()
        currentDataValue = self.data(1, self._m_datatype).toRectF()
        count = self.childCount()

        for i in range(0, count):

            itemChild = self.child(i)

            if(itemChild == child):
                if(itemChild.data(0, QtCore.Qt.UserRole) == name):

                    if(name == self._m_x1_name):
                        currentDataValue = QtCore.QRectF(doubleValue, currentDataValue.y(), currentDataValue.width(), currentDataValue.height())
                    if(name == self._m_y1_name):
                        currentDataValue = QtCore.QRectF(currentDataValue.x(), doubleValue, currentDataValue.width(), currentDataValue.height())
                    if(name == self._m_x2_name):
                        currentDataValue = QtCore.QRectF(currentDataValue.x(), currentDataValue.y(), doubleValue, currentDataValue.height())
                    if(name == self._m_y2_name):
                        currentDataValue = QtCore.QRectF(currentDataValue.x(), currentDataValue.y(), currentDataValue.width(), doubleValue)

                    self.setData(1, self._m_datatype, currentDataValue)
                    self._m_property.set_value(currentDataValue)
                    break

        print "EnvelopeDataTreeItem::refresh" + " " + str(column) + " " + str(name) + " " + str(value)

