from PyQt4 import QtCore

"""
    Qt::UserRole: The first role that can be used for application-specific purposes.
"""

class EnumDataType:

    String = QtCore.Qt.UserRole + 1
    Integer = QtCore.Qt.UserRole + 2
    StringList = QtCore.Qt.UserRole + 3
    Double = QtCore.Qt.UserRole + 4
    Color = QtCore.Qt.UserRole + 5
    FontSettings = QtCore.Qt.UserRole + 6
    Envelope = QtCore.Qt.UserRole + 7

