from PyQt4 import QtCore
from viewrubberband import ViewRubberBand

class ViewZoomArea(ViewRubberBand):

    def __init__(self, view, cursor, parent = None):
        
        ViewRubberBand.__init__(self, view, parent)
        self.cursor = cursor
        self.__zoomStarted = False
        
    def mousePressEvent(self, e):

        if(e.button() != QtCore.Qt.LeftButton):
            return False

        self.__zoomStarted = True
        self.rect = QtCore.QRectF()

        return ViewRubberBand.mousePressEvent(self,e)

    def mouseMoveEvent(self, e):
        
        if(self.__zoomStarted == False):
            return False

        return ViewRubberBand.mouseMoveEvent(self,e)

    def mouseReleaseEvent(self, e):

        self.__zoomStarted = False

        #Roll back the default tool cursor
        self.view.viewport().setCursor(self.cursor)

        if(e.button() != QtCore.Qt.LeftButton):
            return False

        scene = self.view.scene

        ViewRubberBand.mouseReleaseEvent(self,e)

        rect = self.rubberBand.geometry().normalized()

        #Conversion to world coordinates
        poly = self.view.mapToScene(rect)

        #Updates 
        bounding = poly.boundingRect()
        """
        Zoom In Area:
        Scales the view matrix. The view is scaled according to aspectRatioMode.
        Ensure that the scene rectangle rect fits inside the viewport
        """
        self.view.fitInView(bounding, QtCore.Qt.KeepAspectRatio)
        scene.scaleView = self.view.transform().m11()

        scene.update()

        return True

    
