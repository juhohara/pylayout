from PyQt4 import QtCore
from viewzoom import ViewZoom

class ViewZoomClick(ViewZoom):

    def __init__(self, view, cursor, parent = None):

        ViewZoom.__init__(self, view, parent)
        self.cursor = cursor

    def mousePressEvent(self, e):

        if(e.button() != QtCore.Qt.LeftButton):
            return False

        pt = (e.pos())
        self.applyZoom(pt)

        return True

