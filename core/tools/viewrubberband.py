from PyQt4 import QtGui
from PyQt4 import QtCore
from abstractviewtool import AbstractViewTool

class ViewRubberBand(AbstractViewTool):

    def __init__(self, view, parent = None):
        
        AbstractViewTool.__init__(self, view, parent)
        self.__origin = QtCore.QPoint()
        self.__rect = QtCore.QRectF()
        self.__brush = QtGui.QBrush()
        self.__started = False
        self.__draft = QtGui.QPixmap()
        self.__rubberBand = QtGui.QRubberBand(QtGui.QRubberBand.Rectangle, self.view.viewport())

        self.__pen = QtGui.QPen()

        #Setups the rubber band style
        self.__pen.setStyle(QtCore.Qt.DashLine)
        self.__pen.setColor(QtGui.QColor(100, 177, 216))
        self.__pen.setWidth(2)
        self.__brush.setColor(QtGui.QColor(100, 177, 216, 80))
        
    def mousePressEvent(self, e):

        if(e.button() != QtCore.Qt.LeftButton):
            return False

        self.__origin = e.pos()
        self.__started = True
        self.__rubberBand.setGeometry(QtCore.QRect(self.__origin, QtCore.QSize()))
        self.__rubberBand.show()

        return True

    def mouseMoveEvent(self, e):
        
        if(self.__started == False):
            return False
  
        self.__rect = QtCore.QRect(self.__origin, e.pos()).normalized()
        self.__rubberBand.setGeometry(self.__rect)

        return True

    def mouseReleaseEvent(self, e):
        
        self.__started = False
        self.__rubberBand.hide()

        #Roll back the default tool cursor
        self.view.viewport().setCursor(self.cursor)

        if(e.button() != QtCore.Qt.LeftButton | self.__origin.isNull()):
            return False
  
        return True

    def get_origin(self):
        return self.__origin

    def set_origin(self, v):
        self.__origin = v

    def get_rect(self):
        return self.__rect

    def set_rect(self, v):
        self.__rect = v

    def get_brush(self):
        return self.__brush

    def set_brush(self, v):
        self.__brush = v

    def get_started(self):
        return self.__started

    def set_started(self, v):
        self.__started = v

    def get_draft(self):
        return self.__draft

    def set_draft(self, v):
        self.__draft = v

    def get_rubberBand(self):
        return self.__rubberBand

    def set_rubberBand(self, v):
        self.__rubberBand = v

    def get_pen(self):
        return self.__pen

    def set_pen(self, v):
        self.__pen = v

    # ao ser usado, chama o get ou set
    origin = property(fget=get_origin, fset=set_origin)
    rect = property(fget=get_rect, fset=set_rect)
    brush = property(fget=get_brush, fset=set_brush)
    started = property(fget=get_started, fset=set_started)
    draft = property(fget=get_draft, fset=set_draft)
    rubberBand = property(fget=get_rubberBand, fset=set_rubberBand)
    pen = property(fget=get_pen, fset=set_pen)

    
