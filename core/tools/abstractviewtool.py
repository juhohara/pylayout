from PyQt4 import QtGui
from PyQt4 import QtCore

class AbstractViewTool(QtCore.QObject):

    def __init__(self, view, parent = None):
        
        QtCore.QObject.__init__(self, parent)
        self.__view = view #The QGraphicsView associated with the tool.
        self.__cursor = QtGui.QCursor(QtCore.Qt.BlankCursor) #The default tool cursor.

    def eventFilter(self, watched, e):

        if e.type() == QtCore.QEvent.MouseButtonPress :
            return self.mousePressEvent(e)
        elif e.type() == QtCore.QEvent.MouseMove :
            return self.mouseMoveEvent(e)
        elif e.type() == QtCore.QEvent.MouseButtonRelease :
            return self.mouseReleaseEvent(e)
        elif e.type() == QtCore.QEvent.MouseButtonDblClick :
            return self.mouseDoubleClickEvent(e)
        elif e.type() == QtCore.QEvent.Enter :
            if(self.__cursor.shape() != QtCore.Qt.BlankCursor):
                self.__view.viewport().setCursor(self.__cursor)
            return False
        else:
            return QtCore.QObject.eventFilter(self, watched, e)

    def mousePressEvent(self, e):
        print "AbstractViewTool - mousePressEvent"
        return False

    def mouseMoveEvent(self, e):
        return False

    def mouseReleaseEvent(self, e):
        return False

    def mouseDoubleClickEvent(self, e):
        return False

    def get_view(self):
        return self.__view
  
    def set_view(self, v):
        self.__view = v

    def get_cursor(self):
        return self.__cursor
  
    def set_cursor(self, v):
        self.__cursor = v

    # ao ser usado, chama o get ou set
    view = property(fget=get_view, fset=set_view)
    cursor = property(fget=get_cursor, fset=set_cursor)

    
