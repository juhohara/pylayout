from abstractviewtool import AbstractViewTool

class ViewZoom(AbstractViewTool):

    def __init__(self, view, parent = None):

        AbstractViewTool.__init__(self, view, parent)

    def applyZoom(self, point):

        scene = self.view.scene

        #Conversion to world coordinates
        pt = self.view.mapToScene(point)

        zoomFactor = self.view.get_factor()

        #Zoom Out
        self.view.scale(zoomFactor, zoomFactor)
        scene.scaleView = self.view.transform().m11()
        self.view.centerOn(pt)

        scene.update()