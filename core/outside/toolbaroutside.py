from PyQt4 import QtGui
from PyQt4 import QtCore

class ToolbarOutside(QtGui.QToolBar):

    def __init__(self, view, parent = None):
        super(QtGui.QWidget, self).__init__(parent)

        self._m_view = view
        self.initToolbar()

    """ Float Toolbar """
    def initToolbar(self):

        self.setWindowTitle(u"Toolbar")

        self.createButtons()

        self.addSeparator()

        self.createComboFont()

        self.setAllowedAreas(QtCore.Qt.NoToolBarArea)
        self.setOrientation(QtCore.Qt.Horizontal)
        self.setWindowFlags(QtCore.Qt.Tool | QtCore.Qt.FramelessWindowHint | QtCore.Qt.X11BypassWindowManagerHint)

    def createButtons(self):

        #Left button
        btnLeft = self.createToolButton("Left", "Align on the left", "")
        btnLeft.setCheckable(False)
        # Connect the contentsChange signal to a slot.
        btnLeft.clicked.connect(self.__alignOnLeft)
        alignOnLeft = self.addWidget(btnLeft)

        #Right button
        btnRight = self.createToolButton("Right", "Align on the right", "")
        btnRight.setCheckable(False)
        # Connect the contentsChange signal to a slot.
        btnRight.clicked.connect(self.__alignOnRight)
        alignOnRight = self.addWidget(btnRight)

    def createComboFont(self):

        self._m_cmbFont = QtGui.QComboBox(self)
        self._m_cmbFont.setEditable(False)
        self._m_cmbFont.setInsertPolicy(QtGui.QComboBox.NoInsert)

        list = QtCore.QStringList()
        list.append("Arial")
        list.append("Tahoma")
        list.append("Verdana")

        self._m_cmbFont.addItems(list)
        # Connect the contentsChange signal to a slot.
        self._m_cmbFont.activated.connect(self.__comboFontActivated)
        cmbFont = self.addWidget(self._m_cmbFont)

    def __alignOnLeft(self):
        print "Align on left."
        print "-------------------------"

    def __alignOnRight(self):
        print "Align on Right."
        print "-------------------------"

    def __comboFontActivated(self, index):
        print "Combo Font Activated."
        print "-------------------------"

        list = self._m_view.scene.items()

        count = len(list)

        string = self._m_cmbFont.itemText(index)

        for i in range(count):

            item = list[i]

            if(isinstance(item, GridItem) == True):
                item.set_fontName(string)

        self._m_view.scene.update()

    def createToolButton(self, text, tooltip, iconName ):

        btn = QtGui.QToolButton(self)
        btn.setText(text)
        btn.setGeometry(0,0,10,10)
        btn.setCheckable(True)
        btn.setToolTip(tooltip)

        btn.setIcon(QtGui.QIcon.fromTheme(iconName))

        return btn


