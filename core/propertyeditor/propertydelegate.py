from PyQt4 import QtGui
from PyQt4 import QtCore
from fontdialogeditor import FontDialogEditor
from editorfactory import EditorFactory

"""
    When displaying data in a QListView, QTableView, or QTreeView, the individual items are drawn by a delegate.
    What if you want QListView with more then one text or both? To come up from this problem, Qt has concept called Delegates.

    Delegates: These components provide input capabilities and are also responsible for rendering individual items in some views.
"""


class PropertyDelegate(QtGui.QItemDelegate):

    # Create our own signal
    dataEditorChanged = QtCore.pyqtSignal(object, int, int)

    def __init__(self, paintCustomDataType, parent = None):

        super(PropertyDelegate, self).__init__(parent)
        self._m_paintCustomDataType = paintCustomDataType
        self._m_currentEditor = None
        self._m_currentEditorRow = None
        self._m_currentEditorColumn = None

    """
        Reimplement method from QItemDelegate
    """
    def createEditor(self, parent, option, index):

        self._m_currentEditor = None

        if(index.column() == 0):
            self._m_currentEditor = super(PropertyDelegate, self).createEditor(parent, option, index)
            print "PropertyDelegate::createEditor::colunm: " + str(index.column())
        else:
            self._m_currentEditor = self.createFromFactory(parent, option, index)

            if(self._m_currentEditor != None):

                self._m_currentEditorRow = index.row()
                self._m_currentEditorColumn = index.column()

                # connect signal/slot
                self._m_currentEditor.dataValueChanged.connect(self.onDataValueChanged) #signal/slot
                print "PropertyDelegate::createEditor::currentEditor..."
                print self._m_currentEditor

        return self._m_currentEditor

    """
        Reimplement method from QItemDelegate
    """
    def setEditorData(self, editor, index):

        editor.setEditorData(index)

    """
        Reimplement method from QItemDelegate
    """
    def setModelData(self, editor, model, index):

        value = editor.getValue()
        role = editor.getType() # important because this delegate is using custom data type (role) and obvious not exist in Qt default implementation

        if(index.column() == 1):

            modelData = model.data(index, role)
            indexData = index.data(1)
            if(modelData == indexData):
                print "equals..."
                return

        print "PropertyDelegate::setModelData..."

        model.setData(index, value, role)

    """
        Reimplement method from QItemDelegate
    """
    def updateEditorGeometry(self, editor, option, index):

        editor.setGeometry(option.rect)

    def createFromFactory(self, parent, option, index):

        dict = {'index': index, 'option': option}
        editor = EditorFactory.build(dict, parent)

        if isinstance(editor, FontDialogEditor):

            QtCore.QObject.connect(editor, QtCore.SIGNAL('showDlg()'), self, QtCore.SLOT('buttonClicked()'))

        return editor

    @QtCore.pyqtSlot()
    def buttonClicked(self):

        print "PropertyDelegate::buttonClicked..."

    """
        Reimplement method from QItemDelegate
    """
    def paint(self, painter, option, index):

        if(index.column() == 1):

            if(self._m_paintCustomDataType.paint(painter, option, index)):
                return

        super(PropertyDelegate, self).paint(painter, option, index)

    def onDataValueChanged(self, widget, prop):

        print "PropertyDelegate::onDataValueChanged..."
        print widget
        print self._m_currentEditor

        if(widget == self._m_currentEditor):

            print "PropertyDelegate::onDataValueChanged::equals..."

            # Signal from QItemDelegate
            self.commitData.emit(widget) # call setEditorData()

            # Emit our own signal.
            self.dataEditorChanged.emit(prop, self._m_currentEditorRow, self._m_currentEditorColumn)


