from PyQt4 import QtGui
from PyQt4 import QtCore
from core.enumdatatype import EnumDataType
from core.propertyeditor.abstracteditor import AbstractEditor
from core.property.property import Property

"""
    This class contains the following signals:
        - dataValueChanged(QtGui.QWidget, object)
"""


class SpinBoxEditor(QtGui.QSpinBox, AbstractEditor):

    # Create our own signal
    dataValueChanged = QtCore.pyqtSignal(QtGui.QWidget, object)

    def __init__(self, modelIndex, parent = None):

        super(SpinBoxEditor, self).__init__(parent)
        AbstractEditor.__init__(self, EnumDataType.Integer)

        # Signal from QSpinBox
        self.valueChanged.connect(self.setValue) # each value changed call setValue

        self.setKeyboardTracking(False) # Disable for don't get interval numbers. Ex: 250, tracking will be: 2., 5., 0., 250
        self.setMaximum(1000) # The default maximum value is 99.99
        self.setAutoFillBackground(True)

        self.setEditorData(modelIndex)

    """Implemented"""
    def setEditorData(self, index):

        variant = index.data(EnumDataType.Integer)
        if (variant.isValid()):

            value, ok = variant.toInt()
            self.setValue(value)

    """Implemented"""
    def getValue(self):

        return self.value()

    def setValue (self, val):

        QtGui.QSpinBox.setValue(self, val)

        prop = Property

        # Emit our own signal.
        self.dataValueChanged.emit(self, prop)

        
