from core.enumdatatype import EnumDataType
from fontdialogeditor import FontDialogEditor
from lineeditor import LineEditor
from spinboxeditor import SpinBoxEditor
from doublespinboxeditor import DoubleSpinBoxEditor

class EditorFactory(object):

    # Create based on enum type:
    def build(dictParams, parent = None):

        index = dictParams['index']

        variant = index.data(EnumDataType.Integer)
        if (variant.isValid()):

            return SpinBoxEditor(index, parent)

        variant = index.data(EnumDataType.Double)
        if (variant.isValid()):

            return DoubleSpinBoxEditor(index, parent)

        variant = index.data(EnumDataType.String)
        if (variant.isValid()):

            return LineEditor(index, parent)

        variant = index.data(EnumDataType.FontSettings)
        if (variant.isValid()):

            return FontDialogEditor(index, parent)

        variant = index.data(EnumDataType.Envelope)
        #if (variant.canConvert(QtCore.QVariant.RectF)):

            #return FontDialogEditor(index, parent)

    build = staticmethod(build)