from PyQt4 import QtGui
from PyQt4 import QtCore
from core.enumdatatype import EnumDataType
from core.propertyeditor.abstracteditor import AbstractEditor
from core.property.property import Property

"""
    This class contains the following signals:
        - showDlg()
        - dataValueChanged(QtGui.QWidget, object)
"""


class FontDialogEditor(QtGui.QWidget, AbstractEditor):

    # Create our own signal
    showDlg = QtCore.pyqtSignal()
    dataValueChanged = QtCore.pyqtSignal(QtGui.QWidget, object)

    def __init__(self, modelIndex, parent = None):

        super(FontDialogEditor, self).__init__(parent)
        AbstractEditor.__init__(self, EnumDataType.FontSettings)

        self._m_font = QtGui.QFont()

        self.setAutoFillBackground(True)

        textLabel = ""

        variant = modelIndex.data(EnumDataType.FontSettings)
        if (variant.isValid()):
            font = QtGui.QFont(variant)
            self._m_font = font
            textLabel = font.family()
            print textLabel

        self.createGroupBox(textLabel)

    def createGroupBox(self, textLabel):

        hlayout = QtGui.QHBoxLayout(self)

        self.setupTreeViewEditorMargin(hlayout)
        self.__label = QtGui.QLabel(textLabel)

        self.__button = QtGui.QToolButton(self)
        self.__button.setSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Ignored)
        self.__button.setFixedWidth(20)
        self.__button.setText("...")
        self.__button.installEventFilter(self)
        self.__button.clicked.connect(self.buttonClicked)

        self.setFocusProxy(self.__button)
        self.setFocusPolicy(self.__button.focusPolicy())

        hlayout.setSpacing(0)
        hlayout.addWidget(self.__label)
        hlayout.addItem(QtGui.QSpacerItem(0, 0, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Ignored))
        hlayout.addWidget(self.__button)

        self.setLayout(hlayout)

    def eventFilter(self, obj, ev):

        if (obj == self.__button):

            if (ev.type() == QtCore.QEvent.KeyRelease) : # Prevent the QToolButton from handling Enter/Escape meant control the delegate

                if(ev.key() == QtCore.Qt.Key_Return):
                    ev.ignore()
                    return True

        return super(FontDialogEditor, self).eventFilter(obj, ev)

    def paintEvent(self, ev):

        opt = QtGui.QStyleOption()
        opt.init(self)
        p = QtGui.QPainter(self)
        self.style().drawPrimitive(QtGui.QStyle.PE_Widget, opt, p, self)

    def setText(self, textLabel):

        self.__label.setText(textLabel)

    def get_text(self):

        return self.__label.text()

    @QtCore.pyqtSlot()
    def buttonClicked(self):

        print "buttonClicked..."

        # Emit our own signal.
        self.showDlg.emit()

        self.getFont()

    def getFont(self):

        print "getFont..."

        font, ok = QtGui.QFontDialog.getFont(self._m_font, self)
        if ok:
         self._m_font = font
        else:
            return

        prop = Property("", font, EnumDataType.Double, self)

        # Emit our own signal.
        self.dataValueChanged.emit(self, prop)

    def setupTreeViewEditorMargin(self, layout):

        decorationMargin = 4

        if (QtGui.QApplication.layoutDirection() == QtCore.Qt.LeftToRight):

            layout.setContentsMargins(decorationMargin, 0, 0, 0)

        else:

            layout.setContentsMargins(0, 0, decorationMargin, 0)

        """Implemented"""
    def setEditorData(self, index):

        variant = index.data(EnumDataType.FontSettings)
        if (variant.isValid()):
            font = QtGui.QFont(variant)
            self._m_font = font
            self.__label.setText(font.family())

    """Implemented"""
    def getValue(self):

        return self.__label.text()

