import abc

class AbstractEditor(object):

    def __init__(self, dataType):

        super(AbstractEditor, self).__init__()
        self._m_dataType = dataType

    def getType(self):

        return self._m_dataType

    """Abstract Method"""
    @abc.abstractmethod
    def setEditorData(self, index):
        """Method documentation"""
        pass

    """Abstract Method"""
    @abc.abstractmethod
    def getValue(self):
        """Method documentation"""
        pass
