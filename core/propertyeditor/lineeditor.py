from PyQt4 import QtGui
from PyQt4 import QtCore
from core.enumdatatype import EnumDataType
from core.propertyeditor.abstracteditor import AbstractEditor
from core.property.property import Property

"""
    This class contains the following signals:
        - dataValueChanged(QtGui.QWidget, object)
"""


class LineEditor(QtGui.QLineEdit, AbstractEditor):

    # Create our own signal
    dataValueChanged = QtCore.pyqtSignal(QtGui.QWidget, object)

    def __init__(self, modelIndex, parent = None):

        super(LineEditor, self).__init__(parent)
        AbstractEditor.__init__(self, EnumDataType.String)

        self.setAutoFillBackground(True)

        self.setEditorData(modelIndex)

    """Implemented"""
    def setEditorData(self, index):

        variant = index.data(EnumDataType.String)
        if (variant.isValid()):

            self.setText(variant.toString())

    """Implemented"""
    def getValue(self):

        return self.text()







