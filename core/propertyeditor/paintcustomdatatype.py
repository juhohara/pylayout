from PyQt4 import QtGui
from PyQt4 import QtCore
from core.enumdatatype import EnumDataType

"""
    Convert QVariant and paint the right way a custom data type.
"""

class PaintCustomDataType(object):

    def __init__(self, parent = None):

        super(PaintCustomDataType, self).__init__()

    def paint(self, painter, option, index):

        foundDataType = False

        variant = index.data(EnumDataType.Integer)
        if (variant.isValid()):
            value = self.intData(variant)
            painter.drawText(option.rect, QtCore.Qt.AlignLeft, value)
            return True

        variant = index.data(EnumDataType.Double)
        if (variant.isValid()):
            value = self.doubleData(variant)
            painter.drawText(option.rect, QtCore.Qt.AlignLeft, value)
            return True

        variant = index.data(EnumDataType.String)
        if (variant.isValid()):
            value = self.stringData(variant)
            painter.drawText(option.rect, QtCore.Qt.AlignLeft, value)
            return True

        variant = index.data(EnumDataType.FontSettings)
        if (variant.isValid()):
            value = self.fontSettingsData(variant)
            painter.drawText(option.rect, QtCore.Qt.AlignLeft, value)
            return True

        variant = index.data(EnumDataType.Envelope)
        if(variant.isValid()):
            value = self.envelopeData(variant)
            painter.drawText(option.rect, QtCore.Qt.AlignLeft, value)
            return True

        return foundDataType

    def intData(self, variant):

        value, found = variant.toInt()
        return QtCore.QString.number(value)

    def doubleData(self, variant):

        value, found = variant.toDouble()
        return QtCore.QString.number(value)

    def stringData(self, variant):

        return variant.toString()

    def fontSettingsData(self, variant):

        font = QtGui.QFont(variant)
        return font.family()

    def envelopeData(self, variant):

        rect = variant.toRectF()

        return QtCore.QString("[(%1, %2), %3 x %4]").arg(QtCore.QString.number(rect.x()))\
                                                    .arg(QtCore.QString.number(rect.y()))\
                                                    .arg(QtCore.QString.number(rect.width()))\
                                                    .arg(QtCore.QString.number(rect.height()))
