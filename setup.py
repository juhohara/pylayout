from distutils.core import setup

setup(
    name='PyLayout',
    version='1.0',
    packages=['core', 'core.tools', 'core.outside', 'core.datatree', 'core.property', 'core.propertyeditor', 'item',
              'example'],
    url='',
    license='',
    author='Juliana Hohara de Souza Coelho',
    author_email='',
    description='Prototype in Python/PyQt4 for new features and functions to be added in the Layout module of the TerraLib 5.'
)
